///**
// * 
// */
//package com.cariq.toolkittest.www.serviceImpl;
//
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import com.connectiq.www.json.FuelPumpJson;
//import com.connectiq.www.service.FuelPumpService;
//
///**
// * @author santosh
// *
// */
//@Service
//public class FuelPumpServiceImpl implements FuelPumpService {
//
//	@Override
//	public FuelPumpJson[] showFuelPumps() {
//		
//		RestTemplate restTemplate = new RestTemplate();
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		HttpEntity<String> request = new HttpEntity<>(headers);
//		ResponseEntity<FuelPumpJson[]> responseEntity = restTemplate.exchange(
//				"https://rtlddpd.hpcl.co.in/dealerwbnew/resources/generic/getdealers?vendorid=CARIQ&key=L78436zahOcAmE6vuGoTDqqKquRfiR&curr_lat=72.883507&curr_long=19.095148&sw_lat=18.978342&sw_long=72.6885&ne_lat=19.211933&ne_long=73.078514",
//				HttpMethod.GET, request, FuelPumpJson[].class);
//		return responseEntity.getBody();
//	}
//}
