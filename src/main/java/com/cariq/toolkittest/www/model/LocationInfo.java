package com.cariq.toolkittest.www.model;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * @author rohini
 *
 */
public class LocationInfo {
	
	String name;
	
	String description;
	
	double latitude;
	
	double longitude;
	
	GenericJSON attributes;

	public LocationInfo(String name, String description, double latitude, double longitude,
			GenericJSON attributes) {
		super();
		this.name = name;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
		this.attributes = attributes;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public GenericJSON getAttributes() {
		return attributes;
	}

	public GenericJSON toJSON() {
		GenericJSON retVal = new GenericJSON();
		retVal.put("Name", name);
		retVal.put("Description", description);
		retVal.put("Latitude", latitude);
		retVal.put("Longitude", longitude);
		retVal.putAll(attributes);
		return retVal;
	}

}
