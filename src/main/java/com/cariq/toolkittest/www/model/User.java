package com.cariq.toolkittest.www.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * The Class User.
 */
public class User implements UserDetails {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The users. */
	private static Map<String, User> users;

	/** The user name. */
	private String userName;

	/** The password. */
	private String password;

	/** The authorities. */
	private List<Authority> authorities;

	static {
		users = new HashMap<String, User>();
		users.put("connect",
				new User("connect", "b640a0ce465fa2a4150c36b305c1c11b", Arrays.asList(new Authority("ROLE_ADMIN"))));
	}

	/**
	 * Instantiates a new user.
	 *
	 * @param userName
	 *            the user name
	 * @param password
	 *            the password
	 * @param authorities
	 *            the authorities
	 */
	public User(final String userName, final String password, final List<Authority> authorities) {
		this.userName = userName;
		this.password = password;
		this.authorities = authorities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getAuthorities(
	 * )
	 */
	@Override
	public final Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getPassword()
	 */
	@Override
	public final String getPassword() {
		return this.password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getUsername()
	 */
	@Override
	public final String getUsername() {
		return this.userName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isAccountNonExpired()
	 */
	@Override
	public final boolean isAccountNonExpired() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isAccountNonLocked()
	 */
	@Override
	public final boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isCredentialsNonExpired()
	 */
	@Override
	public final boolean isCredentialsNonExpired() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isEnabled()
	 */
	@Override
	public final boolean isEnabled() {
		return true;
	}

	/**
	 * Gets the user.
	 *
	 * @param userName
	 *            the user name
	 * @return the user
	 */
	public static final User getUser(final String userName) {
		return users.get(userName);
	}
}
