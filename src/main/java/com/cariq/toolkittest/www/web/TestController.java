package com.cariq.toolkittest.www.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.utils.CarIQAPI;

@CarIQPublicAPI(name = "CarIQ Connect" , description = "Everything related to CarIQ Connect")
@Controller
@RequestMapping("/tests")
public class TestController {

	@CarIQPublicAPIMethod(description = "Display Message" , responseClass = Object.class)
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Map<String, String> getTestMsg() {
		try (CarIQAPI _getTestMsg = new CarIQAPI("CARIQ_getTestMsg")) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Hi. This project work in under Progress...!");
			return map;
		}
	}
}
