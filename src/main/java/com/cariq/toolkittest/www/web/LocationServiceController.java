//package com.cariq.toolkittest.www.web;
//
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.cariq.toolkit.publicapi.CarIQPublicAPI;
//import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
//import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
//import com.cariq.toolkit.util.CarIQAPI;
//import com.cariq.toolkit.util.Utils;
//import com.connectiq.www.json.FuelPumpJson;
//import com.connectiq.www.service.FuelPumpService;
//import com.connectiq.www.service.LocationService;
//
//@CarIQPublicAPI(name = "Location Service", description = "Everything related to Location Service")
//@RequestMapping("/locationservice")
//@Controller
//public class LocationServiceController {
//
//	@Autowired
//	FuelPumpService fuelPumpService;
//
//	LocationService locationService;
//
//	@CarIQPublicAPIMethod(description = "Get Registered Services",responseClass = HttpServletResponse.class)
//	@RequestMapping(value = "/enlist", method = RequestMethod.GET)
//	@ResponseBody
//	public Object enlist(HttpServletResponse response) {
//		try (CarIQAPI _enlist = new CarIQAPI("CARIQ_enlist")) {
//			return locationService.getServiceDefinition();
//		} catch (Exception e) {
//			Utils.handleException(e);
//		}
//		return null;
//	}
//
//	@CarIQPublicAPIMethod(description = "Fuel Pump Location Infromation", responseClass = FuelPumpJson.class)
//	@RequestMapping(value = "/hpcl", method = RequestMethod.GET)
//	@ResponseBody
//	public FuelPumpJson[] getFuelPumpInfo() {
//		return fuelPumpService.showFuelPumps();
//	}
//
//	@CarIQPublicAPIMethod(description = "Get Service of your interest", responseClass = HttpServletResponse.class)
//	@RequestMapping(value = "/get/{service}", method = RequestMethod.GET)
//	@ResponseBody
//	public Object getService(
//			@CarIQPublicAPIParameter(name = "service", description = "Type of Service")@PathVariable String service, HttpServletResponse response) {
//		try (CarIQAPI _getService = new CarIQAPI("CARIQ_getService")) {
//			return locationService.getServiceDefinition(service);
//		} catch (Exception e) {
//			Utils.handleException(e);
//		}
//		return null;
//	}
//
//	@CarIQPublicAPIMethod(description = "Find service in your vicinity", responseClass = HttpServletResponse.class)
//	@RequestMapping(value = "/fetch/{serviceName}/{latitude}/{longitude}/{region}/{pageNumber}/{pageSize}", method = RequestMethod.GET)
//	@ResponseBody
//	public Object fetch(
//			@CarIQPublicAPIParameter(name = "serviceName", description = "Service Name")@PathVariable String serviceName,
//			@CarIQPublicAPIParameter(name = "latitude", description = "Latitude")@PathVariable double latitude,
//			@CarIQPublicAPIParameter(name = "longitude", description = "Longitude")@PathVariable double longitude,
//			@CarIQPublicAPIParameter(name = "region", description = "region")@PathVariable double region,
//			@CarIQPublicAPIParameter(name = "pageNumber", description = "Page Number")@PathVariable("pageNumber") int pageNumber,
//			@CarIQPublicAPIParameter(name = "pageSize", description = "Page Size")@PathVariable("pageSize") int pageSize, HttpServletResponse response) {
//		try (CarIQAPI _fetch = new CarIQAPI("CARIQ_fetch")) {
//			return locationService.fetch(serviceName, latitude, longitude, region, pageNumber, pageSize);
//		} catch (Exception e) {
//			Utils.handleException(e);
//		}
//		return null;
//	}
//}