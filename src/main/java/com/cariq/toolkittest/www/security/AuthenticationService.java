package com.cariq.toolkittest.www.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.cariq.toolkittest.www.model.User;

/**
 * The Class AuthenticationService.
 */
public class AuthenticationService implements UserDetailsService {

	/**
	 * Instantiates a new authentication service.
	 */
	public AuthenticationService() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	public final UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		return User.getUser(username);
	}

}
