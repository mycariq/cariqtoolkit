/**
 * 
 */
package com.cariq.toolkit;

import java.util.Date;

/**
 * @author hrishi
 * Version History (Replicated in README.md too)
 * 1.0.1, 6-March-2017 - First Port
 * 1.0.2, 8-March-2017 - Add Anomaly,Action logging and publicAPI fix for Boolean
 * 1.0.3, 9-March-2017 - Add RestAPIClientService for calling REST API
 */
public class ToolkitVersion {
	static String majorVersion = "1";
	static String minorVersion = "0";
	static String buildNumber = "3";
	static Date dateCode = new Date(2017, 3, 9);
	public static String getMajorVersion() {
		return majorVersion;
	}
	public static String getMinorVersion() {
		return minorVersion;
	}
	public static String getBuildNumber() {
		return buildNumber;
	}
	public static Date getDateCode() {
		return dateCode;
	}
}
