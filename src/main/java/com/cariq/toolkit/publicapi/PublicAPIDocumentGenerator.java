/**
 * 
 */
package com.cariq.toolkit.publicapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import com.cariq.toolkit.publicapi.swagger.SwaggerPublicAPIDefinition;
import com.cariq.toolkittest.www.web.TestController;

/**
 * @author hrishi
 *
 */
public class PublicAPIDocumentGenerator {
	static PublicAPIDocumentGenerator _instance;

	@Resource(name = "configProperties")
	private Properties configProperties;

	private SwaggerPublicAPIDefinition publicAPI;
	private SwaggerPublicAPIDefinition internalAPI;

	static List<Class<?>> controllers = new ArrayList<Class<?>>();

	static {
//		controllers.add(CarController.class);
//		controllers.add(AlertController.class);
//		controllers.add(DynamicShareController.class);
//		controllers.add(CarIQSettingController.class);
//		controllers.add(TripController.class);
//		controllers.add(UserSettingsController.class);
//		controllers.add(DriveController.class);
//		controllers.add(DriverController.class);
//		controllers.add(FleetController.class);
//		controllers.add(CarIQExporterController.class);
//		controllers.add(CarIQCompositeAPIController.class);
//		controllers.add(CarIQLoaderController.class);
//		controllers.add(BagicAdminController.class);
//		controllers.add(WorkItemController.class);
//		controllers.add(JobController.class);
//		controllers.add(DeviceController.class);
//		controllers.add(DocumentController.class);
//		controllers.add(FuelController.class);
//		controllers.add(PidController.class);
//		controllers.add(ReportController.class);
//		controllers.add(ServiceHistoryController.class);
//		controllers.add(W4IQController.class);
//		controllers.add(CQBadgeController.class);
//		controllers.add(ActivityController.class);
		controllers.add(TestController.class);

	}

	/**
	 * 
	 */
	public PublicAPIDocumentGenerator() {
		PublicAPIBuilder builder = new PublicAPIBuilder();

		// from configurator, load the Controllers from comma separated list - for now, hardcoded list in static block

		for (Class<?> controller : controllers) {
			builder.add(controller);
		}

		builder.init();
		builder.process();
		builder.postProcess();

		publicAPI = builder.getPublicAPIDefinition();
		internalAPI = builder.getInternalAPIDefinition();
	}

	/**
	 * @return
	 */
	public static PublicAPIDocumentGenerator getInstance() {
		//		// Temp - for debugging
		//		return new PublicAPIDocumentGenerator();
		//		@TODO Temp
		if (_instance == null) {
			_instance = new PublicAPIDocumentGenerator();
		}

		return _instance;
	}

	/**
	 * @return
	 */
	public SwaggerPublicAPIDefinition getPublicAPI() {
		return publicAPI;
	}

	/**
	 * @return
	 */
	public SwaggerPublicAPIDefinition getInternalAPI() {
		return internalAPI;
	}

}
