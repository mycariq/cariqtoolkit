/**
 * 
 */
package com.cariq.toolkit.publicapi;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hrishi
 *
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface CarIQPublicAPIParameter {
	String name ();
	String description ();
}
