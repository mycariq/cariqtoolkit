/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

/**
 * @author hrishi
 *
 */
public class Delete extends HttpMethod {

	public Delete(String summary, String description, String operationId) {
		super(summary, description, operationId);
	}

}
