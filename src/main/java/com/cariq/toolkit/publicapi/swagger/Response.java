/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hrishi
 *
 */
public class Response {

	private String description;
	private Definition schema;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public Definition getSchema() {
		return schema;
	}

	public void setSchema(Definition schema) {
		this.schema = schema;
	}

	/**
	 * @param description
	 */
	public Response(String description) {
		this.description = description;
	}

	/**
	 * @param responseDescription
	 * @param typeDefName
	 */
	public Response(String description, Definition def) {
		this(description);
		schema = def;
	}

	/**
	 * @return
	 */
	public static Map<String, Response> getDefaultResponses() {
		Map<String, Response> defaultResponses = new HashMap<String, Response>();
		
		// Fill in the default values.
		defaultResponses.put("400", new Response("Bad Request!"));
		defaultResponses.put("401", new Response("Unauthorized!"));
		defaultResponses.put("403", new Response("Access to the specified resource has been forbidden!"));
		defaultResponses.put("404", new Response("Not Found!"));
		defaultResponses.put("408", new Response("Request Timeout!"));
		defaultResponses.put("500", new Response("Internal Server Error!"));
		defaultResponses.put("503", new Response("Service Unavailable!"));

		
		return defaultResponses;
	}

}
