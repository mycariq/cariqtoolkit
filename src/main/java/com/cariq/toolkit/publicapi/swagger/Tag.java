/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

/**
 * @author hrishi
 *
 */
public class Tag {
	private String name;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param name
	 * @param description
	 */
	public Tag(String name, String description) {
		this.name = name;
		this.description = description;
	}
}