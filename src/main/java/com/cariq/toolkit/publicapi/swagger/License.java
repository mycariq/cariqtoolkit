/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

/**
 * @author hrishi
 *
 */
public class License {
	private String name;
	private String url;

	/**
	 * @param name
	 * @param url
	 */
	public License(String name, String url) {
		this.name = name;
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
