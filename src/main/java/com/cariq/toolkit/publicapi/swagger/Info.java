/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;


/**
 * @author hrishi
 *
 */
public class Info {
	private String description;
	private String version;
	private String title;
	private String termsOfService;
	private Contact contact;
	private License license;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTermsOfService() {
		return termsOfService;
	}

	public void setTermsOfService(String termsOfService) {
		this.termsOfService = termsOfService;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}

	public static Info getCarIQInfo() {
		return CarIQInfo;
	}

	public static void setCarIQInfo(Info carIQInfo) {
		CarIQInfo = carIQInfo;
	}

	/**
	 * @param description
	 * @param version
	 * @param title
	 * @param termsOfService
	 * @param contact
	 * @param license
	 */
	public Info(String title, String version, String description, String termsOfService, Contact contact,
			License license) {
		this.description = description;
		this.version = version;
		this.title = title;
		this.termsOfService = termsOfService;
		this.contact = contact;
		this.license = license;
	}

	public static Info CarIQInfo = new Info(
			"CarIQ Public API",
			"0.1", "CarIQ Platform Public API. Captures all the interesting ways you can use the Platform. It is a work in Progress", "", new Contact("hrishi@mycariq.com"),
			new License("CarIQ", "http://www.mycariq.com"));
}

