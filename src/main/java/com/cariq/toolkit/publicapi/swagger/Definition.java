/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hrishi
 *
 */
public class Definition {

	private String type;
	private Map<String, PropertyAttr> properties = new HashMap<String, PropertyAttr>();
	private Xml xml;

	/**
	 * @param type
	 * @param xml
	 */
	public Definition(String type, Xml xml) {
		this.type = type;
		this.xml = xml;
	}

	public Definition addProperty(String propertyName, PropertyAttr attr) {
		properties.put(propertyName, attr);
		return this;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, PropertyAttr> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, PropertyAttr> properties) {
		this.properties = properties;
	}

	public Xml getXml() {
		return xml;
	}

	public void setXml(Xml xml) {
		this.xml = xml;
	}

	

}
