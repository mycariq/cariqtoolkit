/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author hrishi
 *
 */
public class PropertyAttrEnum extends PropertyAttr {

	private List<String> enumProps = new ArrayList<String>();

	/**
	 * @param type
	 * @param format
	 */
	public PropertyAttrEnum(String type, String format) {
		super(type, format);
	}

	public PropertyAttrEnum addEnumProp(String enm) {
		enumProps.add(enm);
		return this;
	}

	@JsonProperty("enum")
	public List<String> getEnumProps() {
		return enumProps;
	}

	@JsonProperty("enumProps")
	public void setEnumProps(List<String> enumProps) {
		this.enumProps = enumProps;
	}

}
