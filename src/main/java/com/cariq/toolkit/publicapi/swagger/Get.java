/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

/**
 * @author hrishi
 *
 */
public class Get extends HttpMethod {

	public Get(String summary, String description, String operationId) {
		super(summary, description, operationId);
	}

}
