package com.cariq.toolkit.publicapi;

import com.cariq.toolkit.publicapi.swagger.SwaggerPublicAPIDefinition;

public interface ApiDocService {

	/**
	 * @return
	 */
	SwaggerPublicAPIDefinition download();
	
	Object testResponse();

	/**
	 * @param equalsIgnoreCase
	 * @return
	 */
	SwaggerPublicAPIDefinition generate(boolean internal);
}
