package com.cariq.toolkit.utils.batch;


public interface Batch {
	/**
	 * How much of this batch is filled
	 * 
	 * @return
	 */
	int getCurrentSize();

	/**
	 * Preallocated size of this batch
	 * 
	 * @return
	 */
	int getSize();

	/**
	 * This is not the usable function - it is to establish connection of Batch
	 * with Processor
	 * 
	 * @return
	 */
	BatchProcessor getBatchProcessor();

	/**
	 * add will add the object in the batch. When the batch is full, it
	 * automatically processes it the new addition will generate a new batch so
	 * that adder need not do explicit switch
	 * 
	 * @param obj
	 * @return
	 */
	Batch insert(BatchInsertable obj);

	/**
	 * add will add the object in the batch. When the batch is full, it
	 * automatically processes it the new addition will generate a new batch so
	 * that adder need not do explicit switch
	 * 
	 * @param obj
	 * @return
	 */
	Batch update(BatchInsertable obj);
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	String getName();
	
	/**
	 * Process the batch even if not full on request
	 */
	public void processBatch();
}
