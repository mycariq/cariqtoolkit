package com.cariq.toolkit.utils.batch;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class BatchMerger.
 */
public class BatchMergerFactory extends BatchRunnerFactory {

	@Override
	BatchRunner getBatchRunner(List<BatchObject> batchContents) {
		return new BatchMerger(batchContents);
	}

	private static class BatchMerger extends BatchRunner {
		static CarIQLogger logger = BatchHelper.getLogger().getLogger("BatchMerger");

		/**
		 * Instantiates a new batch Merger.
		 *
		 * @param batchContents
		 *            the batch contents
		 */
		public BatchMerger(List<BatchObject> batchContents) {
			super(batchContents);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = Exception.class)
		public void run() {
			try (ProfilePoint _BatchMerger_run = ProfilePoint.profileActivity("ProfAction_BatchMerger_run")) {
				logger.debug("[CALLED],[run],run()");
				List<BatchObject> batchContents = getBatchContents();

				if (batchContents == null || batchContents.isEmpty())
					return;
				logger.info("[INFO],[run],merging models by calling persistDB()");
				for (BatchObject model : batchContents) {
					try (ProfilePoint _mergeDB = ProfilePoint.profileAction("ProfAction_mergeDB")) {
						logger.info("[INFO],[run],Merging " + model.getClass().getCanonicalName());
						model.getObj().mergeDB();
					}
				}
				logger.info("[INFO],[run],Batch merged");
			}
		}
	}
}
