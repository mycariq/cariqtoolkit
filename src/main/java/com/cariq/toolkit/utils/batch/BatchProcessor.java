package com.cariq.toolkit.utils.batch;

import java.util.List;

/**
 * The Interface BatchProcessor.
 */
public interface BatchProcessor {

	/**
	 * Execute.
	 *
	 * @param batchContents
	 *            the batch contents
	 */
	void execute(List<BatchObject> batchContents);

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	String getName();
}
