package com.cariq.toolkit.utils.batch;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.profile.ThroughputCalculator;

/**
 * The Class BatchPersister.
 */
public class BatchPersisterFactory extends BatchRunnerFactory {

	@Override
	BatchRunner getBatchRunner(List<BatchObject> batchContents) {
		return new BatchPersister(batchContents);
	}

	private static class BatchPersister extends BatchRunner {
		static CarIQLogger logger = BatchHelper.getLogger().getLogger("BatchPersister");
		
		// Throughput Calculator to be used for all instances on all threads of this class
		private static ThroughputCalculator batchPersisterThroughput = new ThroughputCalculator("BatchPersister", Utils.PACKET_THROUGHPUT_THRESHOLD);

		/**
		 * Instantiates a new batch persister.
		 *
		 * @param batchContents
		 *            the batch contents
		 */
		public BatchPersister(List<BatchObject> batchContents) {
			super(batchContents);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = Exception.class)
		public void run() {
			try (ProfilePoint _BatchPersister_run = ProfilePoint.profileActivity("ProfAction_BatchPersister_run")) {
				logger.debug("[CALLED],[run],run()");
				List<BatchObject> batchContents = getBatchContents();
				if (batchContents == null || batchContents.isEmpty())
					return;
				logger.info("[INFO],[run],Persisting models by calling persistDB()");
				for (BatchObject model : batchContents) {
					try (ProfilePoint _persistDB = ProfilePoint.profileAction("ProfAction_persistDB")) {
						//logger.info("[INFO],[run],Persisting " + model.getClass().getCanonicalName());
						model.getObj().persistDB();
					}
				}
				
				// Increment throughput by batchcontents Size
				batchPersisterThroughput.increment(batchContents.size());

				logger.info("[INFO],[run],Batch persisted");
			}
		}
	}
}
