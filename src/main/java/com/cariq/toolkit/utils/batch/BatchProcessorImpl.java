package com.cariq.toolkit.utils.batch;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

// TODO: Auto-generated Javadoc
/**
 * The Class BatchProcessorImpl.
 */
public class BatchProcessorImpl implements BatchProcessor {

	/** The executor service. */
	private ThreadPoolExecutor executorService;

	/** The logger. */
	private CarIQLogger logger;

	private String name;
	
	private BatchRunnerFactory runnerFactory;

	/**
	 * Instantiates a new batch processor impl.
	 *
	 * @param threadSize
	 *            the thread size
	 */
	public BatchProcessorImpl(String name, int threadSize, BatchRunnerFactory runnerFactory) {
		executorService = (ThreadPoolExecutor) Executors
				.newFixedThreadPool(threadSize);
		this.name = name;
		this.runnerFactory = runnerFactory;
		logger = BatchHelper.getLogger().getLogger("BatchProcessor-" + name);
	}

	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.batch.BatchProcessor#execute(java.util.List)
	 */
	@Override
	public void execute(List<BatchObject> batchContents) {
		if (batchContents == null || batchContents.size() == 0)
			return;
		
		try (ProfilePoint _BatchProcessor_execute = ProfilePoint
				.profileActivity("ProfAction_BatchProcessor_execute")) {
			logger.debug("[CALLED],[execute],\"batchContents="
					+ ((batchContents == null) ? null : batchContents.size())
					+ "\"");
			logger.info("[INFO],[execute],"
					+ ((isThreadAvailable() ? "Thread is available."
							: "Thread is not available. Waiting for thread......")));
			while (!isThreadAvailable()) {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					Utils.logException(logger, e, "While sleeping thread");
					throw new RuntimeException(e);
				}
			}
			logger.info("[INFO],[execute], Persisting batch.");
			executorService.execute(runnerFactory.getBatchRunner(batchContents));
		}
	}

	/**
	 * Checks if is thread available.
	 *
	 * @return true, if is thread available
	 */
	private boolean isThreadAvailable() {
		return executorService.getActiveCount() < executorService
				.getMaximumPoolSize();
	}
}
