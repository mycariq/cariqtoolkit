package com.cariq.toolkit.utils.batch;

import org.apache.log4j.Logger;

import com.cariq.toolkit.utils.CSVLogger;
import com.cariq.toolkit.utils.CarIQLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class BatchHelper.
 */
public class BatchHelper {

	/** The logger. */
	public static CarIQLogger logger = new CSVLogger(Logger.getLogger("SQLBATCH"), "SQLBATCH");

	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public static CarIQLogger getLogger() {
		return logger;
	}
}
