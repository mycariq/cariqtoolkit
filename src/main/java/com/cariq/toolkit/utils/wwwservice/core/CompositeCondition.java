/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import com.cariq.toolkit.utils.wwwservice.core.condition.And;
import com.cariq.toolkit.utils.wwwservice.core.condition.Or;

/**
 * @author Abhijit
 *
 */
public abstract class CompositeCondition extends Condition {
	public CompositeCondition(Condition first, Condition second) {
		super();
		this.first = first;
		this.second = second;
	}

	Condition first;
	Condition second;

	/**
	 * @return the first
	 */
	public Condition getFirst() {
		return first;
	}

	/**
	 * @return the second
	 */
	public Condition getSecond() {
		return second;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.utils.wwwservice.core.Condition#collectParameters(java.
	 * util.Set)
	 */
	@Override
	public void collectParameters(Set<String> params) {
		first.collectParameters(params);
		second.collectParameters(params);
	}

	/**
	 * Load class from json
	 * 
	 * @param type
	 * @param conditionJson
	 * @return
	 */
	public static Condition fromJson(ConditionType type,
			ConditionJson conditionJson) {
		CompositeConditionJson compositeConditionJson = (CompositeConditionJson) conditionJson;
		Condition first = Condition.fromJson(compositeConditionJson.getFirst());
		Condition second = Condition.fromJson(compositeConditionJson
				.getSecond());

		switch (type) {
		case Or:
			return new Or(first, second);

		case And:
			return new And(first, second);

		default:
			break;
		}

		return null;
	}


	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.Condition#toJson()
	 */
	@Override
	public ConditionJson2 toJson() {
//		CompositeConditionJson retval = new CompositeConditionJson(getType());
//		retval.setFirst(first.toJson());
//		retval.setSecond(second.toJson());
//
//		return retval;
		ConditionJson2 json = new ConditionJson2();
		StringBuilder builder = new StringBuilder();
		builder.append(first.toJson().getConditionStr());
		builder.append(" AND ");
		builder.append(second.toJson().getConditionStr());
		json.setConditionStr(builder.toString());
		return json;
	}
	
	/************************************************************
	 * 
	 * Composite Condition Json
	 * 
	 * @author Abhijit
	 *
	 */
	public static class CompositeConditionJson extends ConditionJson {
		ConditionJson first;
		ConditionJson second;

		/**
		 * Paramless constructor for BootStrapping from JSon
		 */
		public CompositeConditionJson() {
			super();
		}

		public CompositeConditionJson(ConditionType conditionType) {
			super(conditionType);
		}

		/**
		 * @return the first
		 */
		public ConditionJson getFirst() {
			return first;
		}

		/**
		 * @param first
		 *            the first to set
		 */
		@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.EXTERNAL_PROPERTY, property="type")
		public void setFirst(ConditionJson first) {
			this.first = first;
		}

		/**
		 * @return the second
		 */
		public ConditionJson getSecond() {
			return second;
		}

		/**
		 * @param second
		 *            the second to set
		 */
		@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.EXTERNAL_PROPERTY, property="type")
		public void setSecond(ConditionJson second) {
			this.second = second;
		}
	}

}
