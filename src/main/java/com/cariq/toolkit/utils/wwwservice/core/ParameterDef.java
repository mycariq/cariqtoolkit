package com.cariq.toolkit.utils.wwwservice.core;

/**
 * Queriable parameter definition. Definition has name and type.
 * 
 * @author HVN
 *
 */
public class ParameterDef {
	public enum ParameterType {
		ARITHMETIC, LITERAL, DATE, UNKNOWN
	}

	String name;
	String internalValue;
	ParameterType type;
	String[] tokens;

	public ParameterDef(String name, String internalValue, ParameterType type) {
		super();
		this.name = name;
		this.internalValue = internalValue;
		this.type = type;
		tokens = RegexpHelper.tokenize(internalValue);
	}

	public ParameterDef(String name, String internalValue) {
		this(name, internalValue, ParameterType.UNKNOWN);
	}

	/**
	 * Constructor taking name and parameter
	 * 
	 * @param name
	 * @param type
	 */
	public ParameterDef(String name, ParameterType type) {
		this(name, name, ParameterType.UNKNOWN);
	}

	/**
	 * Simplified constructor
	 * 
	 * @param name
	 */
	public ParameterDef(String name) {
		this(name, name, ParameterType.UNKNOWN);
	}

	/**
	 * Value of the parameter - Can be the formula
	 * 
	 * @return
	 */
	public String getParam() {
		return internalValue;
	}

	/**
	 * Value of the parameter - Can be the formula
	 * 
	 * @return
	 */
	public String[] getParams() {
		return RegexpHelper.tokenize(internalValue);
	}

	/**
	 * Name of the parameter
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ParameterDef))
			return false;
		ParameterDef other = (ParameterDef) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * @return the tokens
	 */
	public String[] getTokens() {
		return tokens;
	}

	public static class ParameterDefJson {
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getInternalValue() {
			return internalValue;
		}
		public void setInternalValue(String internalValue) {
			this.internalValue = internalValue;
		}
		String name;
		String internalValue;
	}

	public static ParameterDef fromJson(ParameterDefJson parameterDefJson) {
		return new ParameterDef(parameterDefJson.name,
				parameterDefJson.internalValue);
	}

	public ParameterDefJson toJson() {
		ParameterDefJson parameterDefJson = new ParameterDefJson();
		parameterDefJson.name = this.name;
		parameterDefJson.internalValue = this.internalValue;
		return parameterDefJson;
	}
}
