package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionType;
import com.cariq.toolkit.utils.wwwservice.core.LiteralCondition;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;

public class NotEqual extends LiteralCondition {
	public NotEqual(ParameterDef paramdef, Object value) {
		super(paramdef, value);
	}

	@Override
	public ConditionType getType() {
		return ConditionType.NotEqual;
	}

	@Override
	protected boolean isReallyTrue(Object conditionValue, Object parameterValue) {
		return conditionValue.equals(parameterValue) == false;
	}
	
}
