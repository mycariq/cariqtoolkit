package com.cariq.toolkit.utils.wwwservice.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cariq.toolkit.utils.wwwservice.api.W3FunctionOutput;

public abstract class ArithmeticFunction extends Function {

	public static abstract class FunctionProcessingInfo {
		protected ParameterDef def;
		protected double value;

		public FunctionProcessingInfo(ParameterDef def) {
			super();
			this.def = def;
			this.value = 0.0;
		}

		public abstract Parameter addValue(Tuple tuple, double value);

		public ParameterDef getDef() {
			return def;
		}

		public Object getValue() {
			return value;
		}
	}

	// Map maintains values on ongoing basis
	protected Map<String, FunctionProcessingInfo> processingInfoMap = new HashMap<String, FunctionProcessingInfo>();

	protected W3FunctionOutput doProcess(DataPoint dataPoint) {
		W3FunctionOutput output = null;
		for (ParameterDef paramDef : getParams()) {
			Object val = Parameter.evaluate(paramDef, dataPoint);

			if (null == val) // Parameter not found
				continue;

			try {
				FunctionProcessingInfo info = processingInfoMap.get(paramDef
						.getName());
				if (info == null) {
					info = getProcessingInfo(paramDef);
					processingInfoMap.put(paramDef.getName(), info);
				}

				double value = Double.parseDouble(val.toString());
				Parameter p = info.addValue(dataPoint.getTuple(), value);
				if (null != p) {
					if (output == null)
						output = new W3FunctionOutput(dataPoint.tuple);
					
					output.addParameter(p);
				}
			} catch (NumberFormatException ex) {
				System.out
						.println("Function: " + this.getType() + " : Unable to parse Value for parameter: "
								+ paramDef.getParam()
								+ " Value: "
								+ val.toString());
			}
		}
		return output;
	}

	protected abstract FunctionProcessingInfo getProcessingInfo(ParameterDef paramDef);

	protected List<W3FunctionOutput> doGetOutput() {
		// Single record from the AverageInfo
		W3FunctionOutput out = new W3FunctionOutput(null);
		for (FunctionProcessingInfo info : processingInfoMap.values()) {
			out.addParameter(new Parameter(info.def, info.value));
		}

		ArrayList<W3FunctionOutput> output = new ArrayList<W3FunctionOutput>();
		output.add(out);

		return output;
	}

}
