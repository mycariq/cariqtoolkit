package com.cariq.toolkit.utils.wwwservice.api;

import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.wwwservice.api.W3Query.W3QueryJson;
import com.cariq.toolkit.utils.wwwservice.api.W3QueryResult.W3QueryResultJson;

public class W3QueryAPIImpl implements W3QueryInterface {

	@Override
	public W3QueryResultJson executeQuery(W3QueryJson queryJson) {
		W3Query qry = W3Query.fromJson(queryJson);
		W3QueryResult result = qry.execute();
		CarIQToolkitHelper.logger.debug(result.toString());
		return result.toJson();
	}

}
