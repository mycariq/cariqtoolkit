package com.cariq.toolkit.utils.wwwservice.core.function;

import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction;
import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction.FunctionProcessingInfo;
import com.cariq.toolkit.utils.wwwservice.core.Function.FunctionType;
import com.cariq.toolkit.utils.wwwservice.core.Parameter;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.Tuple;

public class Sum extends ArithmeticFunction {
	static class SumProcessingInfo extends FunctionProcessingInfo {
		ParameterDef def;

		public SumProcessingInfo(ParameterDef def) {
			super(def);
		}

		@Override
		public Parameter addValue(Tuple tuple, double paramValue) {
			value += paramValue;
			return  null;
		}
	}

	@Override
	public FunctionType getType() {
		return FunctionType.SUM;
	}

	@Override
	protected FunctionProcessingInfo getProcessingInfo(ParameterDef paramDef) {
		return new SumProcessingInfo(paramDef);
	}

}
