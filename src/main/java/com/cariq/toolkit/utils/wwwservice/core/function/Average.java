package com.cariq.toolkit.utils.wwwservice.core.function;

import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction;
import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction.FunctionProcessingInfo;
import com.cariq.toolkit.utils.wwwservice.core.Function.FunctionType;
import com.cariq.toolkit.utils.wwwservice.core.Parameter;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.Tuple;

public class Average extends ArithmeticFunction {
	static class AverageProcessingInfo extends FunctionProcessingInfo {
		long countOfRecords;

		public AverageProcessingInfo(ParameterDef def) {
			super(def);
			this.countOfRecords = 0;
		}

		public Parameter addValue(Tuple tuple, double paramValue) {
			value = (value * countOfRecords + paramValue)
					/ (countOfRecords + 1);
			countOfRecords++;
			
			return null;
		}
	}

	@Override
	public FunctionType getType() {
		return FunctionType.AVERAGE;
	}

	@Override
	protected FunctionProcessingInfo getProcessingInfo(ParameterDef paramDef) {
		return new AverageProcessingInfo(paramDef);
	}
}
