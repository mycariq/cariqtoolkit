package com.cariq.toolkit.utils.wwwservice.core;


/**
 * To Restrict the range of the results by specifying page and rowid restriction
 * @author Abhijit
 *
 */
public class QueryRangeRestriction {
	public final static long UNKNOWN_ROW_NUM = -1;
	public final static long UNKNOWN_ROW_ID = -1;
	long minRowNum = UNKNOWN_ROW_NUM, maxRowNum = UNKNOWN_ROW_NUM;
	long minRowId = UNKNOWN_ROW_ID, maxRowId = UNKNOWN_ROW_ID;
	
	public void setRowNumberRange(long startRowNum, long endRowNum ) {
		this.minRowNum = Math.min(startRowNum, endRowNum);
		this.maxRowNum = Math.max(startRowNum, endRowNum);
	}
	public void setRowIdRange(long startRowId, long endRowId) {
		this.minRowId = Math.min(startRowId, endRowId);
		this.maxRowId = Math.max(startRowId, endRowId);
	}
	/**
	 * @return the minRowNum
	 */
	public long getMinRowNum() {
		return minRowNum;
	}
	/**
	 * @return the maxRowNum
	 */
	public long getMaxRowNum() {
		return maxRowNum;
	}
	/**
	 * @return the minRowId
	 */
	public long getMinRowId() {
		return minRowId;
	}
	/**
	 * @return the maxRowId
	 */
	public long getMaxRowId() {
		return maxRowId;
	}
}
