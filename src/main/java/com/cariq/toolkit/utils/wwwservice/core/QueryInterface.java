package com.cariq.toolkit.utils.wwwservice.core;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

public interface QueryInterface {
	/**
	 * Execute the query on the given Tuple and hand over the output in the form of Query Result to process
	 * If outcome is too huge, it will throw exception to suggest to reduce the size
	 * @param dataset
	 * @param ids
	 * @param region
	 * @param timeRange
	 * @param range
	 * @param params
	 * @return
	 */
	QueryResult execute(EntityManager em, String dataset, List<Identity> ids, Region region, TimeRange timeRange,
			QueryRangeRestriction range, Set<String> params) throws EnormousResultException;

}
