package com.cariq.toolkit.utils.wwwservice.core;

import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef.ParameterDefJson;


/**
 * Simple class to capture Key-Value
 * @author HVN
 *
 */
public class Parameter {
	ParameterDef definition;
	Object value;

	/**
	 * Basic Constructor
	 * @param definition
	 * @param value
	 */
	public Parameter(ParameterDef definition, Object value) {
		super();
		this.definition = definition;
		this.value = value;
	}
	
	/**
	 * Simplified Constructor
	 * @param name
	 * @param value
	 */
	public Parameter(String name, Object value) {
		this(new ParameterDef(name), value);
	}

	public Parameter(String name, ParameterDef.ParameterType type, Object value) {
		this(new ParameterDef(name, type), value);
	}

	public String getName() {
		return definition.getName();
	}
	
	public Object getValue() {
		return value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Parameter))
			return false;
		Parameter other = (Parameter) obj;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	/**
	 * get the definition
	 * @return
	 */
	public ParameterDef getDef() {
		return definition;
	}

	public static Object evaluate(ParameterDef paramDef, DataPoint dataPoint) {
		try (ProfilePoint _EvaluateParameter = ProfilePoint
				.profileAction("ProfAction_EvaluateParameter")) {
			try {
				String internalParam = paramDef.getParam();
				Parameter param = dataPoint.get(internalParam);

				if (param != null)
					return param.getValue();

				String expression = paramDef.getParam();
				// System.out.println("Expression: "+ expression);
				return RegexpHelper.evaluateExpression(expression,
						paramDef.getTokens(), dataPoint.getParameters());
			} catch (Exception e) {
				// e.printStackTrace();
				// This may happen
			}
			return null;
		}
	}	
	
	public static class ParameterJson {
		public ParameterDefJson getParameterDef() {
			return parameterDef;
		}
		public void setParameterDef(ParameterDefJson parameterDef) {
			this.parameterDef = parameterDef;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		ParameterDefJson parameterDef;
		Object value;
	}

	public ParameterJson toJson() {
		ParameterJson parameterJson = new ParameterJson();
		parameterJson.parameterDef = definition.toJson();
		parameterJson.value = value;
		return parameterJson;
	}
}
