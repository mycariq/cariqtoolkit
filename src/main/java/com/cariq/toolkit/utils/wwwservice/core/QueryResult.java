package com.cariq.toolkit.utils.wwwservice.core;

public interface QueryResult {
	boolean next();
	boolean isEmpty();
	void close();
	DataPoint getDataPoint();
}
