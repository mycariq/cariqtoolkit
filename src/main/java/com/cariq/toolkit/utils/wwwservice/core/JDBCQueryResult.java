package com.cariq.toolkit.utils.wwwservice.core;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.cariq.toolkit.utils.wwwservice.core.JDBCQuery.Results;
import com.cariq.toolkit.utils.wwwservice.core.JDBCQuery.SelectParams;

public class JDBCQueryResult implements QueryResult {
	static class ResultInfo {
		Results result;
		int rowNum;

		public ResultInfo(Results result, int rowNum) {
			super();
			this.result = result;
			this.rowNum = rowNum;
		}

		public Results getResult() {
			return result;
		}

		public int getRowNum() {
			return rowNum;
		}

		public String getString(String param) {
			return result.getString(param);
		}

		public Long getLong(String param) {
			return result.getLong(param);
		}

		public Double getDouble(String param) {
			return result.getDouble(param);
		}

		public Object getObject(String param) {
			return result.getObject(param);
		}

		public Date getDate(String param) {
			return result.getDate(param);
		}
	}

	List<Object[]> res = null;
	SelectParams selectParams = null;
	Set<String> params = null;
	ResultInfo currentResult = null;
	int rowNum = 0;
	QueryRangeRestriction range;

	public JDBCQueryResult(List<Object[]> res, SelectParams selectParams,
			Set<String> params, QueryRangeRestriction range) {
		super();
		this.res = res;
		this.selectParams = selectParams;
		this.params = params;
		this.range = range;

		// In case of resultset, it should wound upto the rownum
		if (range != null
				&& range.getMinRowNum() != QueryRangeRestriction.UNKNOWN_ROW_NUM)
			rowNum = (int) range.getMinRowNum();
	}

	@Override
	public boolean next() {
		// no result found!
		if (res == null || res.isEmpty())
			return false;

		// Number of rows selected are limited by upper limit
		if (range != null
				&& range.getMaxRowNum() != QueryRangeRestriction.UNKNOWN_ROW_NUM
				&& rowNum > range.getMaxRowNum())
			return false;

		// if there are no records in the list return
		if (res.size() <= rowNum)
			return false;

		currentResult = new ResultInfo(new Results(res.get(rowNum),
				selectParams), rowNum);
		
		rowNum++;
		return true;
	}

	@Override
	public DataPoint getDataPoint() {
		try {
			Tuple t = makeTuple(currentResult);
			DataPoint dataPoint = new DataPoint(t);

			for (String param : params) {
				Object value = currentResult.getObject(param);
				dataPoint.addParameter(new Parameter(
						new ParameterDef(param), value));
			}

			return dataPoint;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Tuple makeTuple(ResultInfo currentResult) throws SQLException {
		long carId = currentResult.getLong("its_car");
		// Date itsTimeStamp =
		// TimeStamp.convertDateTime(currentResult.getDate("its_time_stamp"),
		// currentResult.getTime("its_time_stamp"));
		Date itsTimeStamp = currentResult.getDate("its_time_stamp");
		double latitude = currentResult.getDouble("latitude");
		double longitude = currentResult.getDouble("longitude");

		return new Tuple(new Identity(carId), new TimeStamp(itsTimeStamp),
				new GeoTile(latitude, longitude));

	}

	@Override
	public boolean isEmpty() {
		return res.isEmpty();
	}

	@Override
	public void close() {
	}
}

