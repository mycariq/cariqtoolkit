package com.cariq.toolkit.utils.wwwservice.core.function;

import java.util.List;

import com.cariq.toolkit.utils.wwwservice.api.W3FunctionOutput;
import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction.FunctionProcessingInfo;
import com.cariq.toolkit.utils.wwwservice.core.Function.FunctionType;
import com.cariq.toolkit.utils.wwwservice.core.LiteralFunction;
import com.cariq.toolkit.utils.wwwservice.core.Parameter;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.Tuple;

public class Value extends LiteralFunction {
	static class ValueProcessingInfo extends FunctionProcessingInfo {

		public ValueProcessingInfo(ParameterDef def) {
			super(def);
		}

		@Override
		public Parameter addValue(Tuple tuple, String value) {
			return new Parameter(def, value);
		}
		
	}
	
	@Override
	public FunctionType getType() {
		return FunctionType.VALUE;
	}


	@Override
	protected List<W3FunctionOutput> doGetOutput() {
		return super.getOutputBase();
	}

	@Override
	protected FunctionProcessingInfo getProcessingInfo(ParameterDef paramDef) {
		return new ValueProcessingInfo(paramDef);
	}
}
