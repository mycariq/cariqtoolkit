package com.cariq.toolkit.utils.wwwservice.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cariq.toolkit.utils.wwwservice.core.Parameter;
import com.cariq.toolkit.utils.wwwservice.core.Parameter.ParameterJson;
import com.cariq.toolkit.utils.wwwservice.core.Tuple;
import com.cariq.toolkit.utils.wwwservice.core.Tuple.TupleJson;

/**
 * W3FunctionOutput represents one record in the W3QueryResult.
 * The Record may contain multiple parameter values along with or without Tuple
 * Or single parameter value with or without Tuple
 * @author HVN
 *
 */
public class W3FunctionOutput {
	Map<String, Parameter> parameters = new HashMap<String, Parameter>(); // parameterdef, value
	Tuple tuple;


	/**
	 * Basic Contructor
	 * @param tuple
	 */
	public W3FunctionOutput(Tuple tuple) {
		this.tuple = tuple;
	}

	/**
	 * Add Parameter to the function for query
	 * @param param
	 */
	public void addParameter(Parameter param) {
		parameters.put(param.getName(), param);
	}
	

	/**
	 * get the parameter
	 * @param param
	 * @return
	 */
	public Parameter getParameter(String name) {
		return parameters.get(name);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("{");
		if (tuple != null) // some functions don't return tuple
			builder.append(tuple.toString());
		for (Parameter param : parameters.values()) {
			builder.append(param.getName()).append("=").append(param.getValue()).append(", ");	
		}
		
		builder.append("}");
		
		return builder.toString();
	}
	
	public static class W3FunctionOutputJson {
		public TupleJson getTuple() {
			return tuple;
		}
		public void setTuple(TupleJson tuple) {
			this.tuple = tuple;
		}
		public List<ParameterJson> getParameters() {
			return parameters;
		}
		public void setParameters(List<ParameterJson> parameters) {
			this.parameters = parameters;
		}
		TupleJson tuple;
		List<ParameterJson> parameters = new ArrayList<Parameter.ParameterJson>();
	}

	public W3FunctionOutputJson toJson() {
		W3FunctionOutputJson functionOutput = new W3FunctionOutputJson();
		if (tuple != null)
			functionOutput.tuple = tuple.toJson();
		for (Parameter param : parameters.values()) {
			functionOutput.parameters.add(param.toJson());
		}
		
		return functionOutput;
	}
}
