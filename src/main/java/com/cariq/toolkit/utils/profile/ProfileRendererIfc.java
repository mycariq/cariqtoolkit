package com.cariq.toolkit.utils.profile;

import java.util.UUID;

/**
 * Renderer to render ProfilPoint data. To write data in particular format,
 * different Renderer derivative may be used with DI
 * 
 * @author HVN
 *
 */
public interface ProfileRendererIfc extends BasicCSVRendererIfc {
	void writeMessage(String message);

	void writeHeader(String activityId, String activityName, String actionName,
			String callCount, String totalTimeMsec, String percentage);

	void writeActivityDetails(UUID activityId, String activityName,
			int totalTimeMsec);

	void writeActionDetails(UUID activityId, String actionName, int callCount,
			int totalTimeMsec, double percentage, long size);

	void Flush(); // option to delay write the stream to the external resource
					// (disk/console)
}
