package com.cariq.toolkit.utils.profile.listener;

import com.cariq.toolkit.utils.profile.BasicCSVRendererIfc;
import com.cariq.toolkit.utils.profile.ProfActivity;

/**
 * Abstract factory to create Diagnostic Instruments
 * Instantiated in the beginning
 * @author HVN
 *
 */
public abstract class DiagnosticInstrumentFactory {
	BasicCSVRendererIfc renderer = null;
	
	public DiagnosticInstrumentFactory(BasicCSVRendererIfc renderer) {
		super();
		this.renderer = renderer;
	}

	public BasicCSVRendererIfc getRenderer() {
		return renderer;
	}

	abstract public DiagnosticInstrument getDiagnosticInstrument(ProfActivity rootActivity, String newAction);
}
