package com.cariq.toolkit.utils.profile.listener.instrument;

import java.util.Random;
import java.util.UUID;

import com.cariq.toolkit.utils.profile.BasicCSVRendererIfc;
import com.cariq.toolkit.utils.profile.ProfActivity;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrument;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrumentFactory;

/**
 * This class records Check Points in the long running operation
 * Along with name of the Action, the information about its Start and End is recorded
 * @author hnene
 *
 */
public class CheckPoint extends DiagnosticInstrumentFactory {

	public CheckPoint(BasicCSVRendererIfc renderer) {
		super(renderer);
		renderer.render("CheckPoint", "Event", "Action", "ActionId", "ActivityId", "ActivityName");
	}

	@Override
	public DiagnosticInstrument getDiagnosticInstrument(
			ProfActivity rootActivity, String newAction) {
		return new LoggingInstrument(getRenderer(), rootActivity, newAction);
	}
	
	static class LoggingInstrument extends DiagnosticInstrument {
		UUID activityId = null;
		String activity = null;
		String action = null;
		int actionId;
		static Random r;
		{
			r = new Random();
		}
		
		public LoggingInstrument(BasicCSVRendererIfc renderer, ProfActivity rootActivity, String newAction) {
			super(renderer);
			action = newAction;
			actionId = r.nextInt(1000);
			//actionId = new UUID(new Random(1).nextInt(), 1);
			if (rootActivity != null) {
				activity = rootActivity.getName();
				activityId = rootActivity.getUUID();
				getRenderer().render("CheckPoint","BEGIN", action, String.valueOf(actionId), String.valueOf(activityId), activity);
			}
			else {
				getRenderer().render("CheckPoint", "BEGIN", action, String.valueOf(actionId), String.valueOf(Thread.currentThread().getId()), Thread.currentThread().getName());
			}
		}

		@Override
		public void close() {
			if (activityId != null) {
				getRenderer().render("CheckPoint","END", action, String.valueOf(actionId), String.valueOf(activityId), activity);
			}
			else {
				getRenderer().render("CheckPoint", "END", action, String.valueOf(actionId), String.valueOf(Thread.currentThread().getId()), Thread.currentThread().getName());
			}
			
		}

		@Override
		public String getDescription() {
			return "Logs entry and exit of Action";
		}
		
	}
}
