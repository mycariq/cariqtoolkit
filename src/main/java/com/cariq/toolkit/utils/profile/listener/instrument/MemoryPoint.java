package com.cariq.toolkit.utils.profile.listener.instrument;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.management.ManagementFactory;
import java.util.Properties;

import javax.annotation.Resource;

import com.cariq.toolkit.utils.profile.BasicCSVRendererIfc;
import com.cariq.toolkit.utils.profile.ProfActivity;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrument;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrumentFactory;
import com.cariq.toolkit.utils.profile.listener.instrument.FailurePoint.FailureTime;

/**
 * Instrument to calculate Memory consumption before and after a call.
 * 
 * @author HVN
 *
 */
public class MemoryPoint extends DiagnosticInstrumentFactory {
	static String HEAPDUMP_TEMP_DIRECTORY = null;

	static String[] before_memoryPoints;

	static String[] after_memoryPoints;

	static String memoryprofileType;

	// HVN TODO - do it formally
	@Resource(name = "cariqProfileProperties")
	private static Properties cariqProfileProperties;

	static {
		try {
			memoryprofileType = cariqProfileProperties
					.getProperty("com.cariq.utils.profilepoint.profile.MemoryPoint.Type");
			if (memoryprofileType == null) { // Valid options are SUMMARY or
												// DETAIL
				memoryprofileType = "SUMMARY";
			}
			String beforeFailures = cariqProfileProperties
					.getProperty("com.cariq.utils.profilepoint.profile.MemoryPoint.Before");
			if (beforeFailures != null) {
				before_memoryPoints = beforeFailures
						.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
			}
			String afterFailures = cariqProfileProperties
					.getProperty("com.cariq.utils.profilepoint.profile.MemoryPoint.After");
			if (afterFailures != null) {
				after_memoryPoints = afterFailures
						.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
			}
		} catch (Exception e) {
			before_memoryPoints = null;
			after_memoryPoints = null;
		}
		try {

			Properties properties = cariqProfileProperties;
			String wt_tmp = properties.getProperty("wt.temp");
			HEAPDUMP_TEMP_DIRECTORY = wt_tmp + File.separator + "HeapDumps";

			File d = new File(HEAPDUMP_TEMP_DIRECTORY);
			int i = 0;

			while (d.exists() && !d.isDirectory()) {
				HEAPDUMP_TEMP_DIRECTORY += "_" + ++i;
				d = new File(HEAPDUMP_TEMP_DIRECTORY);
			}

			if (!d.exists()) {
				d.mkdirs();
			}
		} catch (Throwable t) {
			t.printStackTrace(System.err);
			throw new ExceptionInInitializerError(t);
		}
	}

	public MemoryPoint(BasicCSVRendererIfc renderer) {
		super(renderer);
		renderer.render("MemoryPoint", "Description", "Value", "Unit(MB)");

	}

	@Override
	public DiagnosticInstrument getDiagnosticInstrument(
			ProfActivity rootActivity, String newAction) {
		if (before_memoryPoints != null) {
			for (String failPoint : before_memoryPoints) {
				if (newAction.contains(failPoint))
					return new HeapDumpInstrument(newAction, getRenderer(),
							FailureTime.BEFORE);
			}
		}

		if (after_memoryPoints != null) {
			for (String failPoint : after_memoryPoints) {
				if (newAction.contains(failPoint))
					return new HeapDumpInstrument(newAction, getRenderer(),
							FailureTime.AFTER);
			}
		}

		return null;
	}

	static class HeapDumpInstrument extends DiagnosticInstrument {
		static int HEAP_DUMP_COUNT = 0;

		static int MB = 1024 * 1024;

		FailureTime time;

		String action;

		public HeapDumpInstrument(String action, BasicCSVRendererIfc renderer,
				FailureTime t) {
			super(renderer);
			this.action = action;
			this.time = t;

			if (t.equals(FailureTime.BEFORE))
				renderMemoryProfile(getRenderer());
		}

		private void renderMemoryProfile(BasicCSVRendererIfc renderer) {
			renderer.render("MemoryPoint", action, time.toString());
			String name = ManagementFactory.getRuntimeMXBean().getName();
			String pid = name.substring(0, name.indexOf("@"));
			if (memoryprofileType.equals("SUMMARY")) {
				summaryMemoryPoint(renderer, pid);
			} else {
				detailsMemoryPoint(renderer, pid);
			}

		}

		/**
		 * This method executes the command 'jmap -histo <pid>' to obtain the
		 * histogram of the process
		 */
		private void summaryMemoryPoint(BasicCSVRendererIfc renderer, String pid) {
			// After that we can start jmap process like this:
			String filename = HEAPDUMP_TEMP_DIRECTORY + "\\memorypoint_"
					+ action + "_" + (HEAP_DUMP_COUNT++) + ".txt";

			// Check if File exists.If it does exist increment the
			// HEAP_DUMP_COUNT to create a new filename.
			while (new File(filename).exists()) {
				filename = HEAPDUMP_TEMP_DIRECTORY + "\\memorypoint_" + action
						+ "_" + (HEAP_DUMP_COUNT++) + ".txt";
			}

			BufferedWriter writer = null;
			try {
				ProcessBuilder builder = new ProcessBuilder("jmap", "-histo",
						pid);
				Process process = builder.start();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(process.getInputStream()));

				writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(filename)));

				String line = null;
				while ((line = reader.readLine()) != null) {
					writer.write(line);
					writer.newLine();
				}

				renderer.render("MemoryPoint", action,
						"Memory HistoGram File Location", filename);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (writer != null) {
						writer.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

		private void detailsMemoryPoint(BasicCSVRendererIfc renderer, String pid) {
			// After that we can start jmap process like this:
			String filename = HEAPDUMP_TEMP_DIRECTORY + "\\memorypoint_"
					+ action + "_" + (HEAP_DUMP_COUNT++) + ".hprof";

			// Check if File exists.If it does exist increment the
			// HEAP_DUMP_COUNT to create a new filename.
			while (new File(filename).exists()) {
				filename = HEAPDUMP_TEMP_DIRECTORY + "\\memorypoint_" + action
						+ "_" + (HEAP_DUMP_COUNT++) + ".hprof";
			}

			String[] cmd = { "jmap", "-dump:file=" + filename, pid };
			try {
				Process p = Runtime.getRuntime().exec(cmd);
				renderer.render("MemoryPoint", action, "Memory Dump Location",
						filename);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void close() {
			if (time.equals(FailureTime.AFTER))
				renderMemoryProfile(getRenderer());

		}

		@Override
		public String getDescription() {
			return "Dumps Heap periodically";
		}

	}

}
