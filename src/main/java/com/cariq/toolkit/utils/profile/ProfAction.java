package com.cariq.toolkit.utils.profile;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

/**
 * <summary> This is for measuing performance of the codeblock under profile We
 * want to find how much time is taken in this action </summary>
 * 
 * @author hnene
 *
 */

public class ProfAction extends Instrument {
	public UUID activityId;
	private String name;
	StopWatch watch;
	private double percentageTotalTime;
	private int callCount;

	// / <summary>
	// / Constructs Action object - not to be constructed directly
	// / </summary>
	// / <param name="name"></param>
	private ProfAction(String name) {
		this.name = name;
		callCount = 1;
		percentageTotalTime = 0;
		watch = new StopWatch();
	}

	UUID getActivityId() {
		return activityId;
	}

	void setActivityId(UUID activityId) {
		this.activityId = activityId;
	}

	// / <summary>
	// / Create the action object, push it into activity stack and start it
	// / </summary>
	// / <param name="actionName"></param>
	// / <param name="rootActivity"></param>
	// / <returns></returns>

	static ProfAction CreateAndStart(String actionName,
			boolean startNewActivity, AtomicReference<ProfActivity> rootActivity) {
		ProfActivity currentActivity = ProfActivity.getCurrentActivity();
		// if no activity is running and we are not asked to start a newActivity
		// - don't proceed - no Action to be created
		if (currentActivity == null && !startNewActivity)
			return null;

		// Manage Activity - Create root action for this activity - if no
		// activity is currently running
		if (currentActivity == null) {
			assert (startNewActivity);

			ProfAction rootAction = new ProfAction(actionName);
			// First action for this activity - create activity object and
			// return
			rootActivity.set(ProfActivity.setCurrentActivity(new ProfActivity(
					rootAction)));
			rootAction.start();
			return rootAction;
		}

		// check if this action is performed in the current Activity's action
		// stack
		// if so, restart it
		ProfAction last = currentActivity.get(actionName);

		if (last != null) {
			if (last.isRunning() == false) // non recursive call
			{
				last.restart();
				return last;
			}

			// recursive action - create new actionname by appending Guid at the
			// end
			actionName += new UUID(new Random(1).nextInt(), 1);

		}
		// add action to activity's action stack
		ProfAction action = new ProfAction(actionName);
		currentActivity.add(action);

		action.start();
		return action;
	}

	String getName() {
		return name;
	}

	void start() {
		// start the stopwatch
		watch.start();
	}

	void stop() {
		// stop the stopwatch
		watch.stop();
	}

	boolean isRunning() {
		return watch.isRunning();
	}

	int getElapsedTimeMsec() {
		// time on the stopwatch
		return (int) watch.getElapsedTime();
	}

	double getPecentageTotalTime() {
		return percentageTotalTime;
	}

	private void setPecentageTotalTime(double percentage) {
		percentageTotalTime = percentage;
	}

	int getCallCount() {
		return callCount;
	}

	void evaluateTime(int totalTime) {
		assert (!isRunning());
		if (totalTime != 0)
			setPecentageTotalTime((double) getElapsedTimeMsec()
					/ (double) totalTime * 100);
	}

	private void restart() {
		assert (!isRunning());
		callCount++;
		watch.reStart();
	}
}
