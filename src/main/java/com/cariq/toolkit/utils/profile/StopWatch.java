package com.cariq.toolkit.utils.profile;

/**
 * Class: StopWatch
 * To record timing in miliseconds
 * Contains start, pause/stop, reset functionality
 * @author hnene
 *
 */
public class StopWatch {
	private long elapsedTime=0;
    private long startTime = 0;
    private long stopTime = 0;
    private boolean running = false;

    
    public void start() {
        this.startTime = System.currentTimeMillis();
        this.running = true;
    }

    
    public void stop() {
        this.stopTime = System.currentTimeMillis();
        elapsedTime += stopTime - startTime;
        this.running = false;
    }

    
    /**
     * 
     * @return Elapsed time in milliseconds
     */
    public long getElapsedTime() {
        if (!running)
            return elapsedTime;
        
        return (System.currentTimeMillis() - startTime);
     }
        
    //Elapsed time in seconds
    public long getElapsedTimeSecs() {
    	return getElapsedTime()/1000;
    }
    
    /**
     * 
     * @return whether Stopwatch is running
     */
    public boolean isRunning()
    {
    	return running;
    }

    /**
     * Restart Stopwatch
     */
	public void reStart() {
        if (running)
        	return;
        
        start();
	}
}
