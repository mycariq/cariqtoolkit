package com.cariq.toolkit.utils.profile.listener;

import com.cariq.toolkit.utils.profile.BasicCSVRendererIfc;

/**
 * Abstract base class from which Diagnostic Instrument should be derived
 * @author HVN
 *
 */
public abstract class DiagnosticInstrument implements AutoCloseable {
	BasicCSVRendererIfc renderer = null;
	
	public DiagnosticInstrument(BasicCSVRendererIfc renderer) {
		super();
		this.renderer = renderer;
	}
	
	public BasicCSVRendererIfc getRenderer() {
		return renderer;
	}

	public abstract String getDescription();
	
	@Override
	public void close() {
		// placeholder to enforce definition - of not throwing exception
	}	
}
