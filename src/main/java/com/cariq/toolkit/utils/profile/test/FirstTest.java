package com.cariq.toolkit.utils.profile.test;
import org.junit.Ignore;
import org.junit.Test;

import com.cariq.toolkit.utils.profile.ProfilePoint;

public class FirstTest {
@Ignore
@Test
public void first()
{
	System.out.println("Hello World!");
}

//@Ignore
@Test
public void second()
{
	try (ProfilePoint _mainprof = ProfilePoint.profileActivity("second"))
	{
		try (ProfilePoint _prof = ProfilePoint.profileActivity("Trial1"))
		{
			for (int i = 0; i < 100; i++)
			{
				function1(i%10);
			}
			
		}
		
		try (ProfilePoint _prof = ProfilePoint.profileActivity("Trial2"))
		{
		
			for (int i = 0; i < 100; i++)
			{
				function2(i%10);
			}
		}
	}
}

private void _function(int i, String functionName) {
	try (ProfilePoint _prof = ProfilePoint.profileAction(functionName + "_" + i))
	{
		Thread.sleep(i);
	}
	catch (Exception e) {
		
	}
}

private void function1(int i)  {
	_function(i, "function1");
}

private void function2(int i)  {
	_function(i, "function2");
}

@Test
public void third()
{
	// test for ProfilePoint2.0 - to print stack with function
	try (ProfilePoint _mainprof = ProfilePoint.profileActivity("third"))
	{
		caller1();
		basicSleepMethod(20);
		caller2();
		compositeCaller();
		nestedCaller();
	}	
}

@Test
public void fourth()
{
	// test for ProfilePoint2.0 - to print stack with function
	try (ProfilePoint _mainprof = ProfilePoint.profileActivity("fourth"))
	{
		int f = factorial(3);
		f = factorial(4);
	}	
}

private int factorial(int num) {
	try (ProfilePoint _factorial = ProfilePoint.profileActivity("factorial")) {
		basicSleepMethod(10);
		if (num > 1)
			return num * factorial(num-1);
		
		return 1;
	}
}

private void nestedCaller() {
	try (ProfilePoint _compositeCaller = ProfilePoint.profileAction("ProfAction_nestedCaller")) {
		// TODO Auto-generated method stub
		for (int i = 0; i < 2; i++) {
			caller1();
			caller2();
		}
	}
}

private void compositeCaller() {
	try (ProfilePoint _compositeCaller = ProfilePoint.profileAction("ProfAction_compositeCaller")) {
		// TODO Auto-generated method stub
		for (int i = 0; i < 5; i++) {
			caller1();
			caller2();
		}
	}
}

private void caller2() {
	try (ProfilePoint _caller2 = ProfilePoint.profileAction("ProfAction_caller2")) {
		// TODO Auto-generated method stub
		basicSleepMethod(200);
	}
	
}

private void caller1() {
	// TODO Auto-generated method stub
	try (ProfilePoint _caller1 = ProfilePoint.profileAction("ProfAction_caller1")) {
		basicSleepMethod(10);
	}
	
}

private void basicSleepMethod(int time) {
	try (ProfilePoint _prof = ProfilePoint.profileAction("basicSleepMethod"))
	{
		Thread.sleep(time);
	}
	catch (Exception e) {
		
	}
	}

}
