package com.cariq.toolkit.utils.profile.test;
import org.junit.Test;

import com.cariq.toolkit.utils.profile.ProfilePoint;

public class SizeTest {
	//@Ignore
	@Test
	public void second() throws InterruptedException
	{
		try (ProfilePoint _mainprof = ProfilePoint.profileActivity("second"))
		{
			try (ProfilePoint _prof = ProfilePoint.profileActivity("Trial1", 100))
			{
				for (int i = 0; i < 100; i++)
				{
					Thread.sleep(4*(i%10));
				}
				
			}
			
			try (ProfilePoint _prof = ProfilePoint.profileActivity("Trial2"))
			{
			
				for (int i = 0; i < 100; i++)
				{
					Thread.sleep(5*(i%10));
				}
			}
		}
	}
	
	
}
