package com.cariq.toolkit.utils.profile.listener;

import java.util.Stack;

/**
 * Diagnostic instrument stack - stack of autoclosables
 * 
 * @author HVN
 *
 */
@SuppressWarnings("serial")
public class DiagnosticInstrumentStack extends Stack<DiagnosticInstrument>
		implements AutoCloseable {

	/**
	 * Basic autoclosable implementation Forwards close calls to all the
	 * instruments in the reverse order
	 */
	@Override
	public void close() {
		while (!isEmpty()) {
			DiagnosticInstrument instrument = pop();
			if (instrument != null)
				instrument.close();
		}
	}

}
