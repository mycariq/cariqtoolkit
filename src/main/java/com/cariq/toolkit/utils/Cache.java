package com.cariq.toolkit.utils;


public interface Cache<KEY_TYPE, VALUE_TYPE> {
	VALUE_TYPE get(KEY_TYPE key);
	int getSize();
	void add(KEY_TYPE key, VALUE_TYPE value);
//	DateTime lastAccessTime(KEY_TYPE key);
}
