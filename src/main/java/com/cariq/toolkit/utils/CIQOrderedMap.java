package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Ordered Map to keep order to the map Apache Ordered Map is not Generic hence
 * the new one.
 * 
 * @author Abhijit
 *
 * @param <KEY>
 * @param <VALUE>
 */
public class CIQOrderedMap<KEY, VALUE> {
	Map<KEY, VALUE> map = new HashMap<KEY, VALUE>();
	List<KEY> keys = new ArrayList<KEY>();

	public List<KEY> keyList() {
		return keys;
	}

	/**
	 * Add to list as well as map
	 * 
	 * @param key
	 * @param value
	 */
	public void put(KEY key, VALUE value) {
		if (!keys.contains(key))
			keys.add(key);

		map.put(key, value);
	}

	/**
	 * Get From the map
	 * 
	 * @param key
	 * @return
	 */
	public VALUE get(KEY key) {
		return map.get(key);
	}

	/**
	 * Get Size of the map
	 * 
	 * @return
	 */
	public int size() {
		return keys.size();
	}

	Set<KEY> keySet() {
		return map.keySet();
	}

	public KEY firstKey() {
		return keys.get(0);
	}

	public KEY lastKey() {
		return keys.get(keys.size() - 1);
	}

	public void remove(int index) {
		map.remove(keys.get(index));
		keys.remove(index);
	}
}
