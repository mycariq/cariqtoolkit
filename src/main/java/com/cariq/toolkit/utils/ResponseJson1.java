package com.cariq.toolkit.utils;

import java.util.List;

import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
public class ResponseJson1 {

	/**
     */
	private String type;

	/**
     */
	private String code;
	
	/**
	 */
	private String message;

	/**
     */
	private List<String> errors;
}