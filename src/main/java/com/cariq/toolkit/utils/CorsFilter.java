package com.cariq.toolkit.utils;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.filter.OncePerRequestFilter;

public class CorsFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if (request.getMethod().equals("OPTIONS")) {
			try {
				response.setHeader("Access-Control-Allow-Credentials", "true");
				response.setHeader("Access-Control-Allow-Headers",
						"Authorization,Content-Type,Accept,X-Requested-With,X-GUID");
				response.setHeader("Access-Control-Allow-Methods", "POST,GET,PUT,OPTIONS,DELETE");
				response.setHeader("Access-Control-Max-Age", "3600");
				response.getWriter().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			//			final String authorization = request.getHeader("Authorization");
			//			if (authorization != null && authorization.startsWith("Basic")) {
			//				// Authorization: Basic base64credentials
			//				String base64Credentials = authorization.substring("Basic".length()).trim();
			//				String credentials = new String(Base64.decode(base64Credentials.getBytes()), Charset.forName("UTF-8"));
			//				// credentials = username:password
			//				final String[] values = credentials.split(":", 2);
			//				CarIQToolkitHelper.logger.getLogger("AuthService").debug("BasicAuth:--" + authorization + "--");
			//				CarIQToolkitHelper.logger.getLogger("AuthService")
			//						.debug("UserName:--" + values[0] + "-- Password:--" + values[1] + "--");
			//			}
			filterChain.doFilter(request, response);
		}
	}
}