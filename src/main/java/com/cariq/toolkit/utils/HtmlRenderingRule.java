package com.cariq.toolkit.utils;

public interface HtmlRenderingRule {

	String apply(String key, String value);

	String getKey();
	
	boolean isSatisfied(String value);

}
