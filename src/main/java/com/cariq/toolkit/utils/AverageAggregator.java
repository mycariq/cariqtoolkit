package com.cariq.toolkit.utils;

public class AverageAggregator implements ObjectBuilder<Object>{
	int count;
	double avg;
	@Override
	public ObjectBuilder<Object> configure(String setting, Object value) {
		return this;
	}

	@Override
	public ObjectBuilder<Object> add(String parameter, Object value) {
		return add(value);
	}

	@Override
	public ObjectBuilder<Object> build() {
		return this;
	}
	
	public double getValue() {
		return avg;
	}

	@Override
	public ObjectBuilder<Object> add(Object value) {
		double val = Double.parseDouble(value.toString());
		avg = (avg*count + val)/(count+1);
		count++;
		return this;
	}
}
