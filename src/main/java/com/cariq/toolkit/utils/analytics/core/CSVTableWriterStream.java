package com.cariq.toolkit.utils.analytics.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.profile.ProfilePoint;

public class CSVTableWriterStream extends TableWriterStream {
	List<Record> records = new ArrayList<Record>();
	static final int BATCH_SIZE = 1000;

	public CSVTableWriterStream(String fileName) {
		super(fileName);
	}

	/**
	 * Do Buffered write - flush only when BATCH is full
	 */
	@Override
	public void write(Record record) throws IOException {
		records.add(record);
		if (records.size() >= BATCH_SIZE)
			flush();
	}

	/**
	 * Flush the records into the file and close the handle
	 */
	@Override
	public void flush() throws IOException {
		try (ProfilePoint _CSVTableWriter_Flush = ProfilePoint.profileAction("ProfAction_CSVTableWriter_Flush")) {
			// open file in append mode and write all Records
			File fout = new File(super.fileName);
			FileOutputStream fos = new FileOutputStream(fout, true); // always append
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new OutputStreamWriter(fos));

				//				for (Record record : records) {
				//					bw.write(record.toCSVString());
				//					bw.newLine();
				//				}

				for (int index = 0; index < records.size(); index++) {
					bw.write(records.get(index).toCSVString());
					//if (index < records.size() - 1)
						bw.newLine();
				}

			} finally {
				// close the handle
				if (null != bw)
					bw.close();

				records.clear();
			}
		}
	}

	@Override
	public void close() throws Exception {
		flush();
	}

	/**
	 * Write Header line of the file
	 * 
	 * @TODO avoid code duplication
	 */
	@Override
	public void writeHeader() throws IOException {
		// open file in append mode and write all Records
		File fout = new File(getFileName());
		FileOutputStream fos = new FileOutputStream(fout);

		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			bw.write(toCSVString(getColumns()));
			bw.newLine();
		} finally {
			// close the handle
			if (null != bw)
				bw.close();
		}

	}

	/**
	 * Construct the String from List of columns
	 * 
	 * @param columns
	 * @return
	 */
	private String toCSVString(List<Column> columns) {
		StringBuilder bldr = new StringBuilder();
		for (Column column : columns) {
			bldr.append(column.getName()).append(",");
		}
		return bldr.substring(0, bldr.length() - 1).toString();
	}

}
