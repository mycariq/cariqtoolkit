package com.cariq.toolkit.utils.analytics.core;

import java.io.IOException;
import java.util.List;

import com.cariq.toolkit.utils.ObjectBuilder;


public interface TableProcessor extends AutoCloseable, ObjectBuilder<RecordProcessor> {
	String getWorkerId();
	AnalyticsContext getProcessorContext();
	void process() throws IOException;
	void init() throws IOException;
	
	List<RecordProcessor> getRecordProcessors();
	
	String getInputFile();
	String getOutputFile();
	void setForgiveException(boolean value);
}
