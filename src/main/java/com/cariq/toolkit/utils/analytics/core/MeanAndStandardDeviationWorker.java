package com.cariq.toolkit.utils.analytics.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MeanAndStandardDeviationWorker extends AnalyticsWorker {
	List<String> parameters;
	String inputFile;

	public MeanAndStandardDeviationWorker(String id, String description, List<String> parameters, String inputFile) {
		super(id, description, inputFile);
		this.parameters = parameters;
	}

	@Override
	protected void doExecute() throws Exception {
		int iterationNo = 0;
		String file = getInputFile();
		// First Pass - Skim the data with selected columns
		file = doAnalyticsIteration(getSkimDataProcessor(file, iterationNo), "Skim Data to keep relevant stuff", iterationNo++);
		// Calculate Mean
		file = doAnalyticsIteration(getMeanProcessor(file), "Mean Calculation", iterationNo++);
		// Calculate Standard Deviation
		file = doAnalyticsIteration(getStdDeviationProcessor(file), "Standard Deviation Calculation", iterationNo++);
	}

	private String doAnalyticsIteration(TableProcessor dataProcessor,
			String description, int iterationNumber) throws IOException {
		try (AnalyticsProcessIteration iter = new AnalyticsProcessIteration(dataProcessor, description, iterationNumber)) {
			iter.start();
			return iter.getOutputFile();
		}
	}

	private TableProcessor getSkimDataProcessor(String file, int iteration)
			throws FileNotFoundException {
		TableProcessorImpl dataProcessor = new TableProcessorImpl(getContext(),
				getId(), file, AnalyticsHelper.getFilePath(getId(), iteration));
		List<Column> columns = new ArrayList<Column>();
		for (String param : this.parameters) {
			columns.add(new Column(param));
		}

		getContext().setParam(SkimData.class, SkimData.COLUMN_LIST, columns)
		.setParam(SkimData.class, SkimData.VALIDATOR, new RecordValidator() {
			public boolean validate(Record record) {		
				if (record.getTimeStamp() == null)
					return false;
				if (record.getCarId() == null)
					return false;
				return true;
				}
			}
			);
		
		dataProcessor.add(AnalyticsHelper.RECORD_PROCESSOR, new SkimData(getContext())).build();
		return dataProcessor;
	}

	private TableProcessor getStdDeviationProcessor(String file) throws FileNotFoundException {
		TableProcessor dataProcessor = new TableProcessorImpl(getContext(), getId(), file);
		List<Column> columns = new ArrayList<Column>();
		for (String param : this.parameters) {
			columns.add(new Column(param));
		}

		getContext().setParam(StdDeviationCalculator.class, StdDeviationCalculator.COLUMN_LIST, columns);
		dataProcessor.add(AnalyticsHelper.RECORD_PROCESSOR, new StdDeviationCalculator(getContext())).build();
		return dataProcessor;
	}

	private TableProcessor getMeanProcessor(String file) throws FileNotFoundException {
		TableProcessor dataProcessor = new TableProcessorImpl(getContext(), getId(), file);
		List<Column> columns = new ArrayList<Column>();
		for (String param : this.parameters) {
			columns.add(new Column(param));
		}

		getContext().setParam(MeanCalculator.class, MeanCalculator.COLUMN_LIST, columns);
		dataProcessor.add(AnalyticsHelper.RECORD_PROCESSOR, new MeanCalculator(getContext()));
		return dataProcessor;
	}

	@Override
	public List<CarIQFileDesc> getReportFiles() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("AnalyticsWorker.getReportFiles() is not implemented");
	}
}
