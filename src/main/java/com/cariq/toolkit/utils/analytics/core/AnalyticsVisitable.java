package com.cariq.toolkit.utils.analytics.core;

public interface AnalyticsVisitable {
	void accept(AnalyticsVisitor visitor);

}
