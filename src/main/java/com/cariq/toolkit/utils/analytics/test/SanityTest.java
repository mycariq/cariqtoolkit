package com.cariq.toolkit.utils.analytics.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQZipFile;
import com.cariq.toolkit.utils.ProfilePointLogRenderer;
import com.cariq.toolkit.utils.ProximityComparable;
import com.cariq.toolkit.utils.TopItems;
import com.cariq.toolkit.utils.analytics.core.AnalyticsWorker;
import com.cariq.toolkit.utils.analytics.core.MeanAndStandardDeviationWorker;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class SanityTest {
	@Test
	public void testMean() throws FileNotFoundException {
		ProfilePoint.setRenderer(new ProfilePointLogRenderer());
		try (ProfilePoint _testMean = ProfilePoint
				.profileActivity("ProfActivity_testMean")) {
			List<String> params = new ArrayList<String>(Arrays.asList("speed",
					"rpm"));
			// String csvfile = "D:\\projects\\Architecture\\analytics\\ola.csv";
			String csvfile = "D:\\projects\\Architecture\\analytics\\scorpio_mumbai.csv";
			// String report = "D:\\temp\\analytics\\scorpio_mumbai_mean_report.html";
			AnalyticsWorker worker = new MeanAndStandardDeviationWorker(
					"Test", "Calculate mean speed and rpm", params, csvfile);
			worker.execute();
		}
	}
	
	@Test
	public void testZipFile() throws IOException {
		String zipEntryFile = CarIQFileUtils.getTempFile("zipentry.txt");
		String zipFile = CarIQFileUtils.getTempFile("zipFile.zip");
		
		CarIQFileUtils.saveToFile(zipEntryFile, "Hello World!!");
		CarIQZipFile zip = new CarIQZipFile(zipFile);
		zip.add("first", zipEntryFile);
		zip.build();
		System.out.println(zip);
	}
	
	class Coordinate implements ProximityComparable<Coordinate>{
		int x, y;

		
		public Coordinate(int x, int y) {
			super();
			this.x = x;
			this.y = y;
		}


		@Override
		public int compareTo(Coordinate o) {
			// compare y and give proximity based on x
			if (o.y == y)
				return 0;
			
			if (o.y > y) // return negative proximity
				return 0 - Math.abs(o.x - x);
			
			return  Math.abs(o.x - x);
		}


		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "Coordinate [x=" + x + ", y=" + y + "]";
		}
		
	}
	
	@Test
	public void Top3ProximityBased() {
		TopItems<Coordinate> topItems = new TopItems<Coordinate>(3);
		topItems.add(new Coordinate(1,10));
		topItems.add(new Coordinate(2,15));
		topItems.add(new Coordinate(3,20));
		topItems.add(new Coordinate(4,25));
		topItems.add(new Coordinate(5,23));
		topItems.add(new Coordinate(6,21));
		topItems.add(new Coordinate(7,17));
		topItems.add(new Coordinate(18,57));
		topItems.add(new Coordinate(19,59));

		List<ProximityComparable<Coordinate>> topItmes = topItems.build().getTopItems();
		int index = 0;
		for (ProximityComparable<Coordinate> coordinate : topItmes) {
			System.out.println("Index = " + (index++) + ", CoOrdinate: " + coordinate);
		}
	}
	
	// TODO - load using jaxb factory
//	@Test
//	public void testJaxbLoading() {
//
//		 try {
//
//			File file = new File("D:\\git_repos\\cariq\\src\\main\\resources\\META-INF\\analyticsFactory2.xml");
//			JAXBContext jaxbContext = JAXBContext.newInstance(AnalyticsEngineFactory.class);
//
//			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//			AnalyticsEngineFactory factory = (AnalyticsEngineFactory) jaxbUnmarshaller.unmarshal(file);
//			System.out.println(factory);
//
//		  } catch (JAXBException e) {
//			e.printStackTrace();
//		  }
//	}
}
