package com.cariq.toolkit.utils.analytics.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class TableProcessorImpl implements TableProcessor {
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AnalyticsDataProcessor");
	AnalyticsContext context;
	String workerId;
	String inputFile, outputFile;
	TableReaderStream tableReader;
	TableWriterStream tableWriter;
	List<RecordProcessor> recordProcessors = new ArrayList<RecordProcessor>();
	
	boolean forgiveException = false;

	public TableProcessorImpl(AnalyticsContext context, String workerId, String inputFile)
			throws FileNotFoundException {
		super();
		this.context = context;
		this.workerId = workerId;
		this.inputFile = inputFile;
		outputFile = inputFile;

		TableReaderStream tableReader = new CSVTableReaderStream(inputFile);
		context.put(AnalyticsHelper.TABLE_READER_STREAM, tableReader);
		this.tableReader = tableReader;
	}

	public TableProcessorImpl(AnalyticsContext context, String workerId, String inputFile, String outputFile)
			throws FileNotFoundException {
		this(context, workerId, inputFile);
		if (outputFile == null || outputFile.isEmpty())
			return; 
		
		this.outputFile = outputFile;

		TableWriterStream tableWriter = new CSVTableWriterStream(outputFile);
		context.put(AnalyticsHelper.TABLE_WRITER_STREAM, tableWriter);
		this.tableWriter = tableWriter;
	}
	
	public boolean isForgiveException() {
		return forgiveException;
	}

	@Override
	public void setForgiveException(boolean forgiveException) {
		this.forgiveException = forgiveException;
	}

	@Override
	public String getWorkerId() {
		return workerId;
	}

	@Override
	public AnalyticsContext getProcessorContext() {
		return context;
	}

	@Override
	public void process() throws IOException {
		try (ProfilePoint _DataProcessor_process = ProfilePoint.profileAction("ProfAction_DataProcessor_process")) {
			if (null == tableReader) {
				logger.error("No Data Table available!");
				return;
			}
			doProcess();
		}
	}

	private void doProcess() throws IOException {
		while (tableReader.hasNext()) {
			try {
				Record record = tableReader.getNext();
				if (record.isEmpty()) // empty line in csv file (especially last) - is ok
					continue;
				
				for (RecordProcessor processor : recordProcessors) {
					record = processor.process(record); // processor is free to
														// add fields to it
				}

				// if table writer is specified, write record into it
				if (record != null && tableWriter != null)
					tableWriter.write(record);
			} catch (IOException e) {
				throw e;
			} catch (Exception ex) {
				Utils.logException(logger, ex, "Processing Analytics Record");
				if (!forgiveException)
					Utils.handleException(ex);
			}
		}
	}

	@Override
	public void init() throws IOException {
		for (RecordProcessor processor : recordProcessors) {
			processor.init();
		}

		if (tableWriter != null)
			tableWriter.writeHeader();
	}

	@Override
	public void close() throws Exception {
		for (RecordProcessor processor : recordProcessors) {
			processor.close();
		}

		if (null != tableReader)
			tableReader.close();
		if (null != tableWriter)
			tableWriter.close();
	}

	@Override
	public String getInputFile() {
		return inputFile;
	}

	@Override
	public String getOutputFile() {
		return outputFile;
	}

	@Override
	public TableProcessorImpl configure(String setting, Object value) {
		return this;
	}

	@Override
	public TableProcessorImpl add(String parameter, RecordProcessor recordProcessor) {
		if (parameter == null || recordProcessor == null)
			return this;

		if (parameter == AnalyticsHelper.RECORD_PROCESSOR)
			recordProcessors.add(recordProcessor);

		return this;
	}

	@Override
	public TableProcessorImpl build() {
		return this;
	}

	@Override
	public List<RecordProcessor> getRecordProcessors() {
		return recordProcessors;
	}

	@Override
	public TableProcessorImpl add(RecordProcessor value) {
		return add(AnalyticsHelper.RECORD_PROCESSOR, value);
	}
}
