package com.cariq.toolkit.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Class to capture the fundamental functionality of API Mostly REST interface
 * in our case It is also callable so that it can be invoked in different
 * threads
 * 
 * @author hrishi
 *
 */
public class API implements Callable<Object> {
	// default constructor needed for JSON to initialize
	public API() {
	}

	public API(String type, String url, GenericJSON json) {
		super();
		this.type = type;
		this.url = url;
		this.body = json;
	}

	private static HttpMethod getHttpType(String typeStr) {
		if (typeStr.equalsIgnoreCase("Get"))
			return HttpMethod.GET;
		if (typeStr.equalsIgnoreCase("Post"))
			return HttpMethod.POST;
		if (typeStr.equalsIgnoreCase("Put"))
			return HttpMethod.PUT;
		if (typeStr.equalsIgnoreCase("Delete"))
			return HttpMethod.DELETE;

		return null;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public GenericJSON getBody() {
		return body;
	}

	public void setBody(GenericJSON body) {
		this.body = body;
	}

	String type; // GET, POST, PUT, DELETE
	String url; // URL of the API
	GenericJSON body; // JSON in case it is a non GET API

	Map<String, Object> headerParameters = new HashMap<String, Object>();

	// String auth; // Authentication to be applied to the API

	@Override
	public Object call() throws Exception {
		if (getHttpType(type).equals(HttpMethod.GET)) {
			return invoke().getBody();
		}

		return "Only GET APIs are supported to be run in parallel!"; // only
	}

	public API setAuth(String auth) {
		return addHeader("Authorization", auth);
	}

	public API addHeader(String param, Object value) {
		headerParameters.put(param, value);
		return this;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public ResponseEntity<Object> invoke() throws Exception {
		// create header from authentication
		HttpHeaders headers = new HttpHeaders();
		for (String key : headerParameters.keySet()) {
			headers.add(key, headerParameters.get(key).toString());
		}

		// make a REST template
		RestTemplate restTemplate = new RestTemplate();
		if (null == body) {
			try (ProfilePoint _invokeRESTAPI = ProfilePoint.profileActivity("ProfAction_invokeRESTAPI")) {
				HttpEntity<String> request = new HttpEntity<String>(headers);
				ResponseEntity<Object> response = restTemplate.exchange(url, getHttpType(type), request, Object.class);
				return response;
			}
		}

		// body is not null, actualy create the entity with JSON or file
		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(getMultiValueMap(body),
				headers);
		ResponseEntity<Object> response = restTemplate.exchange(url, getHttpType(type), request, Object.class);
		return response;
	}

	private MultiValueMap<String, Object> getMultiValueMap(GenericJSON json) {
		MultiValueMap<String, Object> retval = new LinkedMultiValueMap<String, Object>();
		for (String key : json.keySet()) {
			retval.add(key, json.get(key));
		}
		
		return retval;
	}

}
