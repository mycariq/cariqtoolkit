package com.cariq.toolkit.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface BeanConstructionAttr {
	/**
	 * Whether the attribute is optional
	 * @return
	 */
	boolean optional() default false;

	/**
	 * Class of the object
	 * @return
	 */
	Class<?> clazz() default Object.class;
}
