/**
 * 
 */
package com.cariq.toolkit.utils;

/**
 * @author hrishi
 *
 */
public class HtmlValueIdentityRenderer implements HtmlValueRenderer {

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.HtmlValueRenderer#toHtmlString(java.lang.String, java.lang.String)
	 */
	@Override
	public String toHtmlString(String key, String value) {
		return value;
	}

}
