package com.cariq.toolkit.utils.cdn;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

/**
 * Azure driver for CDN - uses connection string from Config.properties
 * @author hrishi
 *
 */
public class AzureCDNDriver implements CDNDriver {
	CloudBlobClient serviceClient;
	String url;
	String username;
	String password;
	
	@Override
	public void initialize(String url, String username, String password) throws Exception {
		this.url = url;
		this.username = username;
		this.password = password;
		
		CloudStorageAccount account = CloudStorageAccount.parse(url);
		serviceClient = account.createCloudBlobClient();
	}

	@Override
	public String getUrl() {
		return url;
	}
	
	@Override
	public String getUserName() {
		return username;
	}
	
	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void uploadFile(String containerName, String fileName, InputStream inputStream, String contentType,
			long length) throws Exception {
		// Container name must be lower case.
		CloudBlobContainer container = serviceClient.getContainerReference(containerName);
		container.createIfNotExists();
		
		// Upload the file.
		CloudBlockBlob blob = container.getBlockBlobReference(fileName);
		blob.getProperties().setContentType(contentType);
		blob.upload(inputStream, length);
	}

	/**
	 * Delete file
	 */
	@Override
	public void deleteFile(String containerName, String fileName) throws Exception {
		// Container name must be lower case.
		CloudBlobContainer container = serviceClient.getContainerReference(containerName);
		if (container == null || !container.exists()) 
			return;

		// delete file.
		CloudBlockBlob blob = container.getBlockBlobReference(fileName);
		if (blob == null || !blob.exists())
			return;
		
		blob.deleteIfExists();		
	}

	/**
	 * Create Stream and upload file
	 */
	@Override
	public void uploadFile(String containerName, String fileName, String localFilePath, String contentType)
			throws Exception {
		File localFile = new File(localFilePath);
		uploadFile(containerName, fileName, new FileInputStream(localFile), contentType, localFile.length());
	}
	
	/**
	 * Any driver specific cleanup
	 */
	@Override
	public void close() throws IOException {
		serviceClient = null;
	}
}
