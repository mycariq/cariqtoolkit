package com.cariq.toolkit.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;

import com.cariq.toolkit.utils.profile.ProfilePoint;

public class BeanHelper {
//	public static Object create(BeanAttributeBag attributeBag) throws Exception {
//		try (ProfilePoint _createBean = ProfilePoint.profileAction("ProfAction_createBean")) {
//			Class<?> beanClass = attributeBag.getBeanClass();
//			Constructor<?> constructor = beanClass.getConstructor();
//			Object bean = constructor.newInstance();
//			for (String attrName : attributeBag.getAttributeMap().keySet()) {
//				Object val = attributeBag.getAttributeMap().get(attrName);
//				val = resolveObject(val);
//				setProperty(bean, attrName, val);
//			}
//			return bean;
//		}
//	}
	
	static void setProperty(Object bean, String property, Object value) throws IllegalAccessException, InvocationTargetException {
		if (!propertyExists(bean, property))
			throw new RuntimeException("Property named: " + property + " does not exist on Bean Class: " + bean.getClass().getName());
		
		BeanUtils.setProperty(bean, property, value);
	}

//	private static Object resolveObject(Object val) throws Exception {
//		try (ProfilePoint _resolveObject = ProfilePoint.profileAction("ProfAction_resolveObject")) {
//			// If object is a Map, resolve it using Id or signature
//			if (val instanceof Map) { // it must be Object Identity
//				Map identityMap = (Map) val;
//				ModelIdentity identity = new ModelIdentity(identityMap);
//				// ModelIdentity identity = new ModelIdentity((String)identityMap.get("cls"), (String)identityMap.get("id"), (String)identityMap.get("signature"));
//				return CarIQResourceHelper.get(identity);
//			} else if (val instanceof List) { // class and id or signature
//				List identityList = (List) val;
//				ModelIdentity identity = new ModelIdentity(identityList);
//				return CarIQResourceHelper.get(identity);
//			}
//			return val;
//		}
//	}
//
//	public static List<Object> create(List<BeanAttributeBag> attributeBags)
//			throws Exception {
//		try (ProfilePoint _createBeans = ProfilePoint.profileAction("ProfAction_createBeans")) {
//			List<Object> beans = new ArrayList<Object>();
//			// some optimization may be possible, but not sure if it is necessary
//			for (BeanAttributeBag attributeBag : attributeBags) {
//				beans.add(create(attributeBag));
//			}
//			return beans;
//		}
//	}

	/**
	 * Check if the attribute bag contains required attributes for constructing
	 * the Class Instance
	 * 
	 * @param attributeBag
	 */
	public static void validateAttributes(BeanAttributeBag attributeBag) {
		try (ProfilePoint _validateAttributes = ProfilePoint.profileAction("ProfAction_validateAttributes")) {
			String errorMessage = "";
			List<String> requiredConstructionAttributes = getConstructionAttributes(attributeBag.getBeanClass(), false);
			List<String> allConstructionAttributes = getConstructionAttributes(attributeBag.getBeanClass(), true);
			List<String> bagAttributes = attributeBag.getAttributeList();
			// make sure attributebag doesn't have anything extra
			bagAttributes.removeAll(allConstructionAttributes);
			for (String attr : bagAttributes) {
				errorMessage = errorMessage + "\n++ additional Attribute: " + attr;
			}
			requiredConstructionAttributes.removeAll(attributeBag.getAttributeList());
			for (String attr : requiredConstructionAttributes) {
				errorMessage = errorMessage + "\n-- missing Required Attribute " + attr;
			}
			if (!errorMessage.isEmpty())
				throw new RuntimeException(errorMessage);
		}
	}

	/**
	 * Get the list of attributes required for construction
	 * 
	 * @param cls
	 * @return
	 */
	public static List<BeanAttribute> getConstructionAttributes(Class<?> cls) {
		try (ProfilePoint _getConstructionAttributes = ProfilePoint.profileAction("ProfAction_getConstructionAttributes")) {
			List<BeanAttribute> list = new ArrayList<BeanAttribute>();
			while (cls != null) {
				for (Field field : cls.getDeclaredFields()) {
					BeanConstructionAttr annotation = field.getAnnotation(BeanConstructionAttr.class);
					if (null == annotation)
						continue;

					list.add(new BeanAttribute(field.getName(), annotation.clazz(), annotation.optional()));
				}
				cls = cls.getSuperclass();
			}
			return list;
		}
	}

	/**
	 * Get the list of attributes required for construction
	 * 
	 * @param cls
	 * @return
	 */
	public static List<String> getConstructionAttributes(Class<?> cls,
			boolean includeOptional) {
		try (ProfilePoint _getConstructionAttributes = ProfilePoint.profileAction("ProfAction_getConstructionAttributes")) {
			List<String> list = new ArrayList<String>();
			while (cls != null) {
				for (Field field : cls.getDeclaredFields()) {
					BeanConstructionAttr annotation = field.getAnnotation(BeanConstructionAttr.class);
					if (null == annotation)
						continue;

					if (annotation.optional() == false || includeOptional)
						list.add(field.getName());
				}
				cls = cls.getSuperclass();
			}
			return list;
		}
	}

//	public static void set(CarIQResource resourceA, String attrName, CarIQResource resourceB) throws IllegalAccessException, InvocationTargetException {
//		setProperty(resourceA, attrName, resourceB);	
//	}
	
	public static void setTime(Object obj, String attrName) throws IllegalAccessException, InvocationTargetException {
		setProperty(obj, attrName, new Date());	
	}
	
	static boolean propertyExists (Object bean, String property) {
	    try (ProfilePoint _propertyExists = ProfilePoint.profileAction("ProfAction_propertyExists")) {
			return PropertyUtils.isReadable(bean, property) && PropertyUtils.isWriteable(bean, property);
		} 
	}

	@SuppressWarnings("unchecked")
	public static <T>  T valueOf(Class<T> cls, String objStr) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		try (ProfilePoint _valueOf = ProfilePoint.profileAction("ProfAction_valueOf")) {
			Method method = cls.getMethod("valueOf", String.class);
			if (method == null)
				throw new RuntimeException("Method: valueOf() not found on class " + cls.getName());
			return (T) method.invoke(null, objStr);
		}
	}

	/**
	 * Populate Attributes in the map
	 * @param resource
	 * @param attributeMap
	 * @throws Exception 

	 */
	public static void populateAttributes(Object bean, Map<String, Object> attributeMap) throws Exception {
		try (ProfilePoint _populateAttributes = ProfilePoint.profileAction("ProfAction_populateAttributes")) {
			List<String> attrNames = getAttributeNames(bean.getClass());
			// using bean utils fill the attributeMap.
			for (String property : attrNames) {
				Object obj = BeanUtils.getProperty(bean, property);
				attributeMap.put(property, obj);
			} 
		}
	}

	public static List<String> getAttributeNames(Class<?> cls) {
		try (ProfilePoint _getAttributeNames = ProfilePoint.profileAction("ProfAction_getAttributeNames")) {
			PropertyDescriptor[] properties = PropertyUtils.getPropertyDescriptors(cls);
			List<String> attrNames = new ArrayList<String>();
			for (PropertyDescriptor descr : properties) {
				attrNames.add(descr.getName());
			}
			Collections.sort(attrNames);
			return attrNames;
		}
	}
	
	public static Map<String, Class<?>> getAttributeProperties(Class<?> cls) {
		try (ProfilePoint _getAttributeNames = ProfilePoint.profileAction("ProfAction_getAttributeNames")) {
			PropertyDescriptor[] properties = PropertyUtils.getPropertyDescriptors(cls);
			Map<String, Class<?>> attrProp = new HashMap<String, Class<?>>();
			for (PropertyDescriptor descr : properties) {
				attrProp.put(descr.getName(), descr.getPropertyType());
			}
			return attrProp;
		}
	}
}
