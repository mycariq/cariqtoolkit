package com.cariq.toolkit.utils;

import java.util.Iterator;
import java.util.List;

public interface BatchIterator <T> extends Iterator<List<T>> {
	// get size of the batch given by this iterator
	int getBatchSize();

}
