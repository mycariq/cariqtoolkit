package com.cariq.toolkit.utils.scoring;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cariq.toolkit.utils.Utils;

// Singleton
public class ScoringEngine {
	static String SCORING_ENGINE_FILE = "ScoringEngine.xml";
	static ScoringEngine instance = new ScoringEngine();
	
	Map<String, ScoreDefinition> scoringDefinitionsMap = new HashMap<String, ScoreDefinition>();
//	String name;
	// List<ParameterDefinition> paramDefs = new ArrayList<ParameterDefinition>();

	private ScoringEngine() {
		try {
			// load ScoringEngie from resource
			File scoringEngineFile = new ClassPathResource(SCORING_ENGINE_FILE).getFile();
			// populate ScoringDefinitions
			load(scoringEngineFile);
		} catch (Exception e) {
			Utils.handleException(e);
		}
	}
	
	private void validateName(Node element, String name) throws Exception {
		if (element.getNodeName().equals("name"))
			throw new Exception("Invalid Root Element of ScoringEngine: " + element.getNodeName());

	}
	
	private void load(File scoringEngineFile) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(scoringEngineFile);
				
		//optional, but recommended
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();

		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		Element scoringEngine = doc.getDocumentElement();
		validateName(scoringEngine, "ScoringEngine");

		// Load ScoreDefinitions
		NodeList scoringDefinitions = scoringEngine.getElementsByTagName("ScoreDefinition");
		for (int nodeNo = 0; nodeNo < scoringDefinitions.getLength(); nodeNo++) {
			Node def = scoringDefinitions.item(nodeNo);
			validateName(def, "ScoreDefinition");
			Element scoreingDefElement = (Element) def;

			ScoreDefinition scoringDef = new ScoreDefinition();
			scoringDef.configure("Name", scoreingDefElement.getAttribute("Name"))
			.configure("Min", scoreingDefElement.getAttribute("Min"))
			.configure("Max", scoreingDefElement.getAttribute("Max"));
			
			// Load Parameter Definitions
			NodeList paramDefs = scoreingDefElement.getElementsByTagName("ParameterDefinition");
			for (int paramDefNo = 0; paramDefNo < paramDefs.getLength(); paramDefNo++) {
				Node pDef = paramDefs.item(paramDefNo);
				validateName(pDef, "ParameterDefinition");
				Element pDefElement = (Element) pDef;
				String name = pDefElement.getAttribute("Name");
				String weight = pDefElement.getAttribute("Weight");
				String min = pDefElement.getAttribute("Min");
				String max = pDefElement.getAttribute("Max");
				String defaultValue = pDefElement.getAttribute("Default");
				
				scoringDef.add(new ParameterDefinition(name, Double.parseDouble(weight), Double.parseDouble(min), Double.parseDouble(max), Double.parseDouble(defaultValue)));		
			}
			
			// Add to map
			scoringDef = scoringDef.build();
			scoringDefinitionsMap.put(scoringDef.getName(), scoringDef);
		}
	}

	public static ScoringEngine getInstance() throws Exception {
		if (null == instance)
			instance = new ScoringEngine();
		
		return instance;
	}
	
	public ScoreDefinition getScoreDefinition(String name) {
		return scoringDefinitionsMap.get(name);
	}
//	
//	public ScoringEngine configure(String setting, Object value) {
//		if (setting.equals("NAME"))
//			name = (String) value;
//
//		return this;
//	}
}
