package com.cariq.toolkit.utils.scoring;

public class Parameter {
	ParameterDefinition definition;
	double value;
	
	public Parameter(ParameterDefinition definition, double value) {
		super();
		this.definition = definition;
		this.value = definition.validateValue(value);
	}

	public Parameter(ParameterDefinition def) {
		this(def, def.getDefaultValue());
	}

	public ParameterDefinition getDefinition() {
		return definition;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}	
	
	public double evaluateValue() {
		return value * definition.getWeightage();
	}

	public String getName() {
		return definition.getParameterName();
	}
}
