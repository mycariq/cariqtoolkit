package com.cariq.toolkit.utils.scoring;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.ObjectBuilder;

/**
 * Definition of Scoring for a particular score
 * Depends on Parameter Definitions
 * 
 * @author Abhijit
 *
 */
public class ScoreDefinition implements ObjectBuilder<ParameterDefinition> {
	String name = "UNKNOWN";
	List<ParameterDefinition> parameterDefinitions = new ArrayList <ParameterDefinition>();
	double minScore = 0, maxScore = 10;
	double minRange = 0, maxRange = 0;


	/**
	 * Establish weightage of each Parameter based on total range of score
	 * @param defs
	 */
	private void evaluateRange() {		
		for (ParameterDefinition parameterDef : parameterDefinitions) {
			double weightage = parameterDef.getWeightage();
			if (weightage < 0) {
				minRange += weightage * parameterDef.getMaxValue();
				maxRange += weightage * parameterDef.getMinValue();
			}
			else {
				minRange += weightage * parameterDef.getMinValue();
				maxRange += weightage * parameterDef.getMaxValue();
			}
		}
		// System.out.println("TEMP: Range = " + minRange + "-" + maxRange);
	}

	public String getName() {
		return name;
	}

	public double getMinScore() {
		return minScore;
	}

	public double getMaxScore() {
		return maxScore;
	}

	public List<ParameterDefinition> getParameterDefinitions() {
		return parameterDefinitions;
	}

	/*
	 * Normalize based on range
	 */
	public double normalize(double score) {
		if (score <= minRange)
			return minScore;
		if (score >= maxRange)
			return maxScore;
		
		// y = (x-x1)*(y2-y1)/(x2-x1) + y1
		return minScore + (score - minRange)*(maxScore - minScore)/(maxRange - minRange);
	}

	public ScoreDefinition configure(String setting, Object value) {
		if (setting.equalsIgnoreCase("NAME"))
			name = (String) value;
		if (setting.equalsIgnoreCase("MIN"))
			minScore = Double.parseDouble(value.toString());
		if (setting.equalsIgnoreCase("MAX"))
			maxScore = Double.parseDouble(value.toString());
		
		return this;
	}

	public ScoreDefinition add(ParameterDefinition value) {
		parameterDefinitions.add(value);
		return this;
	}

	public ScoreDefinition add(String parameter, ParameterDefinition value) {
		parameterDefinitions.add(value);
		return this;
	}

	public ScoreDefinition build() {
		evaluateRange();
		return this;
	}
}
