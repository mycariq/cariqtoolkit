package com.cariq.toolkit.utils.scoring;

import java.util.HashMap;
import java.util.Map;

import com.cariq.toolkit.utils.SimpleAttributeMap;
import com.cariq.toolkit.utils.Utils;

public class Scorer {
//	static CarIQLogger logger = CarIQToolkitHelper.getLogger("Scorer");

	String name;
	Map<String, Parameter> params = new HashMap<String, Parameter>();
	ScoreDefinition scoringDefinition;
	public Scorer(ScoreDefinition scoringDefinition) {
		super();
		this.scoringDefinition = scoringDefinition;
		this.name = scoringDefinition.getName();
		prePopulateParameters();
	}
	
	/**
	 * Populate with default values
	 */
	private void prePopulateParameters() {
		for (ParameterDefinition def : scoringDefinition.getParameterDefinitions()) {
			Parameter param = new Parameter(def);
			params.put(param.getName(), param);
		}
	}

	public void addParameters(SimpleAttributeMap paramMap, boolean includeZero) {
		for (String name : paramMap.keySet()) {
			try {
				Double value = Utils.downCast(Double.class, paramMap.get(name));
				if (value == null)
					continue;
				if (includeZero == false && value == 0.0)
					continue;
				
				addParameter(name, value);
			} catch (Exception e) {
				continue;
			}
		}
	}	
	
	
	public void addParameters(SimpleAttributeMap paramMap) {
		addParameters(paramMap, true);
	}
	
	public void addParameter(String name, double value) {
		Parameter param = params.get(name);
		if (null == param) {
			System.out.println("Parameter Definition not found: " + name);
			return;
		}
		
		// logger.debug("Scorer.addParamater: " + name + ": " + value);
		param.setValue(value);
	}
	
	public double evaluate() {
		double score = 0;
		
		for (Parameter param : params.values()) {
			score = score + param.evaluateValue();
		}
		
		return scoringDefinition.normalize(score);
		
	}

	/**
	 * Calculate the factor within the given range provided
	 * based on actual score and range of possible score
	 * @param score
	 * @param scoreBasedRange
	 * @return
	 */
	public double evaluateOverlayFactor(double score, double[] range) {
		if (range == null || range.length != 2)
			Utils.handleException("Range for Overlay is incorrect!!");
		
		double minV = range[0];
		double maxV = range[1];
		double minS = scoringDefinition.getMinScore();
		double maxS = scoringDefinition.getMaxScore();
		
//		logger.debug("Range of Score: { "+ minS + " - " + maxS + " }");
//		logger.debug("Range of Overlay Values: { "+ minV + " - " + maxV + " }");
//		logger.debug("Score: { "+ score + " }");

		
		// 2 point equation of line
		return ((score - minS)*(maxV - minV)/(maxS - minS)) + minV;
	}

}
