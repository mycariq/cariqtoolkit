package com.cariq.toolkit.utils;

public class ObjectCreator<T> {
	private T obj;

	private Class<T> clazzT;

	public ObjectCreator(Class<T> clazzT) {
		this.clazzT = clazzT;
	}

	public T create() {
		if (obj == null) {
			try {
				obj = clazzT.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return obj;
	}

	public T get() {
		return obj;
	}
}
