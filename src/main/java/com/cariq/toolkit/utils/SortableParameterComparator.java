/**
 * 
 */
package com.cariq.toolkit.utils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author hrishi
 *
 */
public class SortableParameterComparator implements Comparator< Map<String, Object>> {
	List<SortableParameter> sortableParams;
	/**
	 * @param sortableParams
	 */
	public SortableParameterComparator(List<SortableParameter> sortableParams) {
		this.sortableParams = sortableParams;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Map<String, Object> first, Map<String, Object> second) {
		// compare first, then second, then third and so on Sortable Parameters
		int comparison = 0; // when in doubt, assume match
		for (SortableParameter sortableParameter : sortableParams) {
			String key = sortableParameter.getName();
			Object firstObject = first.get(key);
			Object secondObject = second.get(key);
			
			// if one doesn't exist
			if (firstObject == null)
				return 1;

			// if one doesn't exist
			if (secondObject == null)
				return -1;
			
			// Downcast to actual classes for correct comparison
			try {
				Comparable firstReal = (Comparable) Utils.downCast(sortableParameter.getClazz(), firstObject);
				Comparable secondReal = (Comparable) Utils.downCast(sortableParameter.getClazz(), secondObject);
				
				comparison =  firstReal.compareTo(secondReal);
				// if they match, go to next parameter, else return
				if (comparison == 0)
					continue;
				
				return comparison;
			} catch (Exception e) {
				return 0; // don't know
			}
		}
		return 0;
	}


}
