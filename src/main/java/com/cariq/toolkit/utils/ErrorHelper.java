/**
 * 
 */
package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author HVN
 *
 */
public class ErrorHelper {
	// special agreement with the firmware about Clean Error Code syntax
	public static final String NO_ERROR="P0000";
	public enum ErrorStatus {ErrorsFound, NoErrorFound, Unknown};

	public static class ErrorStack { // implemented in terms of List<String>
		List<String> errors = new ArrayList<String>(); // by default ErrorList is null

		public ErrorStack(List<String> errorList) { // may be null
			if (errorList != null)
				this.errors = errorList;
			
			// If Error string contains NO_ERROR, it is not error (Ref: Scorpio initial Junk)
			if (errors!= null && errors.contains(NO_ERROR))
				errors = Arrays.asList(NO_ERROR);

		}
		
		public ErrorStack(String errorString) {
			if (errorString != null)
				errors = Arrays.asList(errorString.split("\\|"));
			
			// If Error string contains NO_ERROR, it is not error (Ref: Scorpio initial Junk)
			if (errors!= null && errors.contains(NO_ERROR))
				errors = Arrays.asList(NO_ERROR);
		}
		

		public ErrorStatus getErrorStatus() {
			if (null == errors || errors.isEmpty())
				return ErrorStatus.Unknown; // No error information found in the PID
			
			if (errors.contains(NO_ERROR))
				return ErrorStatus.NoErrorFound;
			
			return ErrorStatus.ErrorsFound;
		}

		/**
		 * Determine union of two stacks
		 * @param other
		 * @return
		 */
		public ErrorStack union(ErrorStack other) {
			return new ErrorStack(union(errors, other.errors));
		}
		
		/*
		 * Find Intersection between this and other stack
		 */
		public ErrorStack intersect(ErrorStack other) {
			return new ErrorStack(intersection(errors, other.errors));
		}
		
		/**
		 * Remove members of other stack from this
		 * @param other
		 * @return
		 */
		public ErrorStack subtract(ErrorStack other) {
			return new ErrorStack(subtract(errors, other.errors));
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (obj == null)
				return false;
			
			if (!(obj instanceof ErrorStack))
				return false;
			
			ErrorStack other = (ErrorStack) obj;
			if (getErrorStatus() != other.getErrorStatus())
				return false;
			
			return equals(errors, other.errors);
		}
		
		/**
		 * Return list of errors
		 * @return
		 */
		public List<String> getErrors() {
			return errors;
		}
		
		/**
		 * Return a string by concatenating the Error Strings
		 */
		public String getAllErrors() {
			// String buffer for String mutation
			StringBuffer retval = new StringBuffer("");
			
			// isFirst pattern to append separator before the error
			boolean isFirst = true;
			for (String error : errors) {
				if (isFirst) {
					retval.append(error);
					isFirst = false;
				}
				else {
					retval.append("|").append(error);
				}
			}
			return retval.toString();
		}

		/**
		 * Union Two generic lists
		 * @param list1
		 * @param list2
		 * @return
		 */
		static private <T> List<T> union(List<T> list1, List<T> list2) {
			Set<T> set = new HashSet<T>();

			set.addAll(list1);
			set.addAll(list2);

			return new ArrayList<T>(set);
		}

		/**
		 * Intersect two lists
		 * @param list1
		 * @param list2
		 * @return
		 */
		static private <T> List<T> intersection(List<T> list1, List<T> list2) {
			List<T> list = new ArrayList<T>();

			for (T t : list1) {
				if (list2.contains(t)) {
					list.add(t);
				}
			}
			return list;
		}
		
		/**
		 * Subtract second from first - instead of removeall
		 * @param list1
		 * @param list2
		 * @return
		 */
		static private <T> List<T> subtract(List<T> list1, List<T> list2) {
			List<T> list = new ArrayList<T>();

			for (T t : list1) {
				if (!list2.contains(t)) {
					list.add(t);
				}
			}
			return list;
		}
		
		static private <T> boolean equals(List<T> list1, List<T> list2) {
			Set<T> set1 = new HashSet<T>();
			set1.addAll(list1);
			Set<T> set2 = new HashSet<T>();
			set2.addAll(list2);
			
			return set1.equals(set2);
		}

		public boolean isEmpty() {
			return errors == null || errors.isEmpty();
		}
	}
}
