package com.cariq.toolkit.utils;

public class HtmlRenderingValue {
	public static final String VALUE_TOKEN = "_VALUE_";
	String renderingTemplate;
	

	/**
	 * @param renderingTemplate
	 */
	public HtmlRenderingValue(String renderingTemplate) {
		this.renderingTemplate = renderingTemplate;
	}

	/**
	 * Apply Rendering to value
	 * @param value
	 * @return
	 */
	public String render(String value) {
		return renderingTemplate.replaceAll(VALUE_TOKEN, value == null? "no data" : value);
	}
	
	public static HtmlRenderingValue HtmlBold = new HtmlRenderingValue("<b>" + VALUE_TOKEN +"</b>");
	public static HtmlRenderingValue HtmlRed = new HtmlRenderingValue("<span style=\"background-color: Orange\">[ " + VALUE_TOKEN +" ]</FONT></span>");
	public static HtmlRenderingValue HtmlYellow = new HtmlRenderingValue("<span style=\"background-color: Yellow\">[ " + VALUE_TOKEN +" ]</FONT></span>");
	public static HtmlRenderingValue HtmlGreen = new HtmlRenderingValue("<span style=\"background-color: LightGreen\">[ " + VALUE_TOKEN +" ]</FONT></span>");

	public static HtmlRenderingValue HtmlEnlarge = new HtmlRenderingValue("<FONT SIZE=\"+2\">" + VALUE_TOKEN +"</FONT>");
	public static HtmlRenderingValue HyperLink(String linkAddress) {
		return new HtmlRenderingValue("<a href=\"" + linkAddress + "\">" + VALUE_TOKEN +"</a>");
	}
}
