package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

/**
 * Conceptual representation of a Day in a TimeZone
 * Day in England has different boundary than Day in India
 * @author Abhijit
 *
 */
public class Day {
	public static final int HOURS_OF_DAY = 24;

	String timeZone = Utils.INDIA_TIME_ZONE;
	Date startTime;
	public Day(Date date, String timeZone) {
		if (timeZone != null)
			this.timeZone = Utils.getTimeZone(timeZone);
		
		startTime = Utils.getStartofDay(date, timeZone);
	}
	public Date getStart() {
		return Utils.getDayStartTimeByItsTime(startTime, timeZone);
	}
	
	public Date getEnd() {
		return Utils.getDayEndTimeByItsTime(startTime, timeZone);
	}
	
	public Date getDayTimeStart() {
		return Utils.getDateSecondsAfter(getStart(), Utils.hourSeconds * 6); // 6 AM
	}
	
	public Date getDayTimeEnd() {
		return Utils.getDateSecondsAfter(getStart(), Utils.hourSeconds * 18); // 6 PM
	}
	
	public Date getNightTimeStart() {
		return getDayTimeEnd();
	}
	public Date getNightTimeEnd() {
		return getEnd();
	}
	
	String getTimeZone() {
		return timeZone;
	}
	
	public Day getPreviousDay() {
		return getDaysBefore(1);
	}
	
	public Day getNextsDay() {
		return getDaysAfter(1);
	}
	
	public Day getDaysAfter(int days) {
		DateTime other = new DateTime(startTime).plusDays(days);
		return new Day(other.toDate(), this.timeZone);
	}
	
	public Day getDaysBefore(int days) {
		DateTime other = new DateTime(startTime).minusDays(days);
		return new Day(other.toDate(), this.timeZone);
	}
	
	public static boolean isWithinDays(Day startDay, Day endDay, Date timeStamp) {
		long startTime = startDay.getStart().getTime();
		long endTime = endDay.getEnd().getTime();
		long dayStart = timeStamp.getTime();
		
		if (dayStart < startTime || dayStart > endTime)
			return false;
		
		return true;
	}
	
	boolean isAfter(Date timeStamp) {
		return getEnd().after(timeStamp);
	}
	boolean isBefore(Date timeStamp) {
		return getStart().after(timeStamp);
	}
	
	boolean isWithin(Date timeStamp) {
		if (isAfter(timeStamp))
			return false;
		
		if (isBefore(timeStamp))
			return false;
		
		return true;
	}
	
	/**
	 * Get days between given time
	 * @param minTime
	 * @param maxTime
	 * @return
	 */
	public static List<Day> getDays(Date minTime, Date maxTime) {
		if (minTime.after(maxTime))
			return getDays(maxTime, minTime); // forgive wrong ordering
		
		List<Day> returnVal = new ArrayList<Day>();
		Day day = new Day(minTime, null);
		while(!day.isBefore(maxTime)) {// add till day frame crosses maxTime
			returnVal.add(day);
			day = day.getNextsDay();
		}
		
		return returnVal;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return Utils.getDateOnly(startTime);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		result = prime * result + ((timeZone == null) ? 0 : timeZone.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Day other = (Day) obj;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		if (timeZone == null) {
			if (other.timeZone != null)
				return false;
		} else if (!timeZone.equals(other.timeZone))
			return false;
		return true;
	}
}
