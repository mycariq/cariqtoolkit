package com.cariq.toolkit.utils.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.cdn.CDNConnection;

@Service
public class FileHandlerServiceImpl implements FileHandlerService {

	// load config properties
	@Resource(name = "configProperties")
	private Properties configProperties;

	@Override
	public String uploadFile(MultipartFile file, String fileName, String cdnName) throws Exception {
		uploadToCDN(new ByteArrayInputStream(file.getBytes()), fileName, cdnName, file.getContentType(),
				file.getSize());
		return fileName;
	}

	@Override
	public void deleteFile(String fileName, String cdnName) throws Exception {
		try (CDNConnection con = CDNConnection.open()) {
			try {
				con.deleteFile(cdnName, fileName);
			} catch (Exception e) {
				Utils.handleException(e);
			}
		}
		//
		//		CloudStorageAccount account = CloudStorageAccount.parse(getStorageConnectionString());
		//		CloudBlobClient serviceClient = account.createCloudBlobClient();
		//
		//		// Container name must be lower case.
		//		CloudBlobContainer container = serviceClient.getContainerReference(cdnName);
		//		container.createIfNotExists();
		//
		//		// Upload an image file.
		//		CloudBlockBlob blob = container.getBlockBlobReference(fileName);
		//		blob.deleteIfExists();
	}
	//
	//	// get storage string
	//	@Override
	//	public String getStorageConnectionString() {
	////		return "DefaultEndpointsProtocol=http;" + "AccountName="
	////				+ configProperties.getProperty("azureBLOBStorageAccountName") + ";AccountKey="
	////				+ configProperties.getProperty("azureBLOBStorageAccountKey");
	//		return null;
	//	}

	@Override
	public void validateFileSize(MultipartFile file, int size) {
		if (file.getSize() > size) {
			Utils.handleException("Attached file must be " + convertByteToMb(size) + " mb or less");
		}
	}

	//	@Override
	//	public void validateFileFormat(MultipartFile file, String[] formats) {
	//		String contentType = file.getContentType().toLowerCase();
	//		for (String format : formats) {
	//			if (contentType.contains(format))
	//				return;
	//		}
	//		if (Arrays.asList(formats).contains("pdf"))
	//			CarIqValidator.handleException("Only Images (JPG/PNG) and PDF files are supported.");
	//		CarIqValidator.handleException("Only Image (JPG/PNG) files are supported.");
	//	}

	// convert bytes to mb
	private double convertByteToMb(int size) {
		return size / Utils.ONE_MB_IN_BYTES;
	}

	//	private void validateFile(MultipartFile imageFile, int fileSize, String[] contentTypes) {
	//
	//		if (imageFile == null)
	//			return;
	//
	//		// validate file format
	//		validateFileFormat(imageFile, fileFormats);
	//
	//		// validate file size
	//		validateFileSize(imageFile, fileSize);
	//	}

	@Override
	public void uploadLocalFile(File file, String fileName, String cdnName) throws Exception {
		InputStream inputStream = new FileInputStream(file);
		//URLConnection.guessContentTypeFromName(file.getName());
		uploadToCDN(inputStream, fileName, cdnName, URLConnection.guessContentTypeFromName(file.getName()),
				file.length());
	}

	@Override
	public void uploadLocalFile(String filePath, String fileName, String cdnName) throws Exception {
		uploadLocalFile(new File(filePath), fileName, cdnName);
	}

	private void uploadToCDN(InputStream inputStream, String fileName, String cdnName, String contentType, long length)
			throws Exception {
		try (CDNConnection con = CDNConnection.open()) {
			try {
				con.uploadFile(cdnName, fileName, inputStream, contentType, length);
			} catch (Exception e) {
				Utils.handleException(e);
			}
		}
	}

	@Override
	public File getLocalFileHandle(MultipartFile file, String fileName) throws IOException {
		// Create a local file 
		String outputFile = CarIQFileUtils.getTempFile(fileName);
		// copy the incoming file to localfile
		InputStream input = file.getInputStream();
		try {
			CarIQFileUtils.copyContents(outputFile, input);
			return new File(outputFile);
		} finally {
			input.close();
		}
	}

	@Override
	public boolean isValidFileContentType(MultipartFile file, ContentType[] contentTypes) {
		boolean flag = false;
		for (ContentType contentType : contentTypes) {
			if (file.getContentType().equals(contentType.toString()))
				flag = true;
		}
		return flag;
	}

	@Override
	public void validateImageFile(MultipartFile file, int fileSize) {
		if (file == null)
			return;
		if (!isValidFileContentType(file, new ContentType[] { ContentType.PNG, ContentType.JPEG }))
			Utils.handleException("Only image (JPEG/PNG) file is supported");
		validateFileSize(file, fileSize);
	}

	@Override
	public void validatePdfFile(MultipartFile file, int fileSize) {
		if (file == null)
			return;
		if (!isValidFileContentType(file, new ContentType[] { ContentType.PDF }))
			Utils.handleException("Only PDF file is supported");
		validateFileSize(file, fileSize);
	}

	@Override
	public void validateImageAndPdfFile(MultipartFile file, int fileSize) {
		if (file == null)
			return;
		if (!isValidFileContentType(file, new ContentType[] { ContentType.PDF, ContentType.PNG, ContentType.JPEG }))
			Utils.handleException("Only image (PNG/JPEG) and PDF file is supported");
		validateFileSize(file, fileSize);

	}
}
