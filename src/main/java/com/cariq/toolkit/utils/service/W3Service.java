/**
 * 
 */
package com.cariq.toolkit.utils.service;

import org.springframework.stereotype.Service;

import com.cariq.toolkit.utils.wwwservice.api.W3Query.W3QueryJson;

/**
 * @author hrishi
 *
 */
@Service
public abstract class W3Service {

	/**
	 * W3Query API - currently exposed through the testController - will move to
	 * appropriate place later
	 * 
	 * @param param - W3Query Json containing the Scope Tuple and Functions to be executed with a condition
	 * @param carIqUser - User executing this Query in the form of a work item
	 * @return
	 */
	public abstract Object executeW3Query(W3QueryJson param);
}
