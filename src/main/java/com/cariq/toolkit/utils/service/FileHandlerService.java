package com.cariq.toolkit.utils.service;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

/**
 * The Interface FileHandlerService.
 */
public interface FileHandlerService {

	public enum ContentType {
		PNG("image/png"), JPEG("image/jpeg"), PDF("application/pdf"), TXT("text/plain"), ZIP("application/zip"), GZIP(
				"application/gzip");
		String value;

		private ContentType(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	/**
	 * Upload file to CDN.
	 *
	 * @param file
	 *            the file
	 * @param fileName
	 *            the file name
	 * @param cdnName
	 *            the cdn name
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public String uploadFile(MultipartFile file, String fileName, String cdnName) throws Exception;

	/**
	 * Delete file from cdn.
	 *
	 * @param fileName
	 *            the file name
	 * @param cdnName
	 *            the cdn name
	 * @throws Exception
	 *             the exception
	 */
	public void deleteFile(String fileName, String cdnName) throws Exception;

	/**
	 * Validate file size.
	 *
	 * @param file
	 *            the file
	 * @param size
	 *            the size
	 */
	public void validateFileSize(MultipartFile file, int size);

	//	/**
	//	 * Validate file format.
	//	 *
	//	 * @param file
	//	 *            the file
	//	 * @param fileFromats
	//	 *            the file fromats
	//	 */
	//	public void validateFileFormat(MultipartFile file, String[] fileFromats);

	//	/**
	//	 * Get storage connection string
	//	 * 
	//	 * @return
	//	 */
	//	public String getStorageConnectionString();

	/**
	 * Validate image file.
	 *
	 * @param file
	 *            the file
	 * @param fileSize
	 *            the file size
	 */
	public void validateImageFile(MultipartFile file, int fileSize);

	/**
	 * Validate pdf file.
	 *
	 * @param file
	 *            the file
	 * @param fileSize
	 *            the file size
	 */
	public void validatePdfFile(MultipartFile file, int fileSize);

	/**
	 * Validate image and pdf file.
	 *
	 * @param file
	 *            the file
	 * @param fileSize
	 *            the file size
	 */
	public void validateImageAndPdfFile(MultipartFile file, int fileSize);

	/**
	 * Upload local file.
	 *
	 * @param file
	 *            the file
	 * @param fileName
	 *            the file name
	 * @param cdnName
	 *            the cdn name
	 * @throws Exception
	 *             the exception
	 */
	public void uploadLocalFile(File file, String fileName, String cdnName) throws Exception;

	/**
	 * Upload local file.
	 *
	 * @param filePath
	 *            the file path
	 * @param fileName
	 *            the file name
	 * @param cdnName
	 *            the cdn name
	 * @throws Exception
	 *             the exception
	 */
	void uploadLocalFile(String filePath, String fileName, String cdnName) throws Exception;

	/**
	 * Gets the local file handle.
	 *
	 * @param file
	 *            the file
	 * @param fileName
	 *            the file name
	 * @return the local file handle
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public File getLocalFileHandle(MultipartFile file, String fileName) throws IOException;

	/**
	 * Checks if is valid file content type.
	 *
	 * @param file
	 *            the file
	 * @param contentTypes
	 *            the content types
	 * @return true, if is valid file content type
	 */
	public boolean isValidFileContentType(MultipartFile file, ContentType[] contentTypes);

}
