package com.cariq.toolkit.utils;

import org.apache.log4j.Logger;

public class CSVLogger implements CarIQLogger {
	Logger logger;
	String logContext; // This is CSV string specifying context of the log

	public CSVLogger(Logger logger, String logContext) {
		super();
		this.logger = logger;
		this.logContext = logContext;
	}

	@Override
	public void fatal(String message) {
		logger.fatal(concat(logContext, message));
	}

	@Override
	public void error(String message) {
		logger.error(concat(logContext, message));
	}

	@Override
	public void warn(String message) {
		logger.warn(concat(logContext, message));
	}

	@Override
	public void info(String message) {
		logger.info(concat(logContext, message));
	}

	@Override
	public void debug(String message) {
		logger.debug(concat(logContext, message));
	}

	@Override
	public CarIQLogger getLogger(String innerContext) {
		return new CSVLogger(logger, concat(this.logContext, innerContext));
	}

	private String concat(String str1, String str2) {
		return str1 + "," + str2;
	}
}
