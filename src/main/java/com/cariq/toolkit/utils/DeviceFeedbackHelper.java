package com.cariq.toolkit.utils;

public class DeviceFeedbackHelper {
	// Device Feedback Category
	public static final String DEVICE_FEEDBACK = "DEVICE_FEEDBACK";

	// Device Feedback Commands
	public static final String WIFI_NAME = "WIFI_NAME";
	public static final String WIFI_PASSWORD = "WIFI_PASSWORD";
	public static final String WIFI_ENCRYPTION = "WIFI_ENCRYPTION";
	public static final String WIFI_SWITCH = "WIFI_SWITCH";
	public static final String WIFI_AUTORESTART = "WIFI_AUTO_RESTART";
	public static final String WIFI_ENCRYPTION_NOPWD = "WIFI_ENCRYPTION_NOPWD";
	public static final String WIFI_ENCRYPTION_WPA2_PSK = "WIFI_ENCRYPTION_WPA2_PSK";
	public static final String WIFI_ENCRYPTION_WPA_TKIP = "WIFI_ENCRYPTION_WPA_TKIP";
	public static final String GPS_FREQUENCY = "GPS_FREQUENCY";
	public static final String SWITCH_MAIN_SERVER = "SWITCH_MAIN_SERVER";
	public static final String SWITCH_STANDBY_SERVER = "SWITCH_STANDBY_SERVER";
	public static final String DEVICE_FEEDBACK_WIFI_AUTORESTART = "DEVICE_FEEDBACK_WIFI_AUTORESTART";

	// Device Feedback Parameter Keys
	public static final String SSID = "SSID";
	public static final String WIFI_ONOFF = "WIFI_ONOFF";
	public static final String PWD = "PWD";
	public static final String FREQUENCY = "FREQUENCY";

	public static final String SERVER_NAME = "SERVER_NAME";
	public static final String PORT = "PORT";
	public static final String APN = "APN";

	public static final String IS_ON = "IS_ON";

	//Mobilize parameter keys
	public static final String MOBILIZE_SWITCH = "MOBILIZE_SWITCH";
	public static final String MOBILIZE_ONOFF = "MOBILIZE_ONOFF";

	//pass key parameter keys
	public static final String PASS_KEY = "PASS_KEY";
}
