package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.ObjectBuilder;

/**
 * JSONable class to represent Query name and parameters
 * @author hrishi
 *
 */

public class CarIQQueryDefinition implements ObjectBuilder<QueryDefinitionAttribute> {

	String queryType;
	String description;
	List<QueryDefinitionAttribute> queryAttributes = null;
	
	public CarIQQueryDefinition(String type, String description) {
		super();
		this.queryType = type;
		this.description = description;
		this.queryAttributes = new ArrayList<QueryDefinitionAttribute>();
	}
	
	public CarIQQueryDefinition(String type, String description, String[] attrList) {
		this(type, description);
		if (attrList == null)
			return;
		
		for (String attr : attrList) {
			queryAttributes.add(new QueryDefinitionAttribute(attr));
		}
	}

	public CarIQQueryDefinition(String type, String description, List<QueryDefinitionAttribute> attrList) {
		this(type, description);
		queryAttributes = attrList;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<QueryDefinitionAttribute> getQueryAttrDefinition() {
		return queryAttributes;
	}
	
	public void setQueryAttrDefinition(List<QueryDefinitionAttribute> queryAttributes) {
		this.queryAttributes = queryAttributes;
	}
	
	// Builder methods
	@Override
	public CarIQQueryDefinition configure(String setting, Object value) {
		if (setting.equals("TYPE"))
			queryType = (String) value;
		else if (setting.equals("DESCRIPTION"))
			description = (String) value;
		
		return this;
	}
	
	@Override
	public CarIQQueryDefinition add(QueryDefinitionAttribute value) {
		queryAttributes.add(value);
		return this;
	}
	
	@Override
	public CarIQQueryDefinition add(String parameter, QueryDefinitionAttribute value) {
		return add(value);
	}
	
	@Override
	public CarIQQueryDefinition build() {
		return this;
	}
}
