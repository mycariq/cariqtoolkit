package com.cariq.toolkit.coreiq.exporter;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.FileHandlerService;

@CarIQPublicAPI(name = "Exporter", description = "Everything related to Exporter")
@RequestMapping("/admin/exporter")
@Controller
public class CarIQExporterController {

	@Autowired
	CarIQExporterService exporterService;

	@Autowired
	FileHandlerService fileHandlerService;

	@Resource(name = "configProperties")
	private Properties configProperties;

	/**
	 * Get All Exporters
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "List of Exporters", responseClass = List.class, internal = true)
	@RequestMapping(value = "/enlist", method = RequestMethod.GET)
	@ResponseBody
	public Object enlistExporters(HttpServletResponse response) {
		try (CarIQAPI _getResource = new CarIQAPI("CARIQ_getExporters")) {
			return exporterService.getExporterDefinitions();
		} catch (Exception e) {
			Utils.handleException(e);
		}
		return null;
	}

	/**
	 * Get Attributes required by a exporter
	 * 
	 * @param exporter
	 * @param response
	 * @return
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "get Details of Exporter", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/get/{exporter}", method = RequestMethod.GET)
	@ResponseBody
	public Object getExporter(
			@CarIQPublicAPIParameter(name = "exporter", description = "the exporter") @PathVariable("exporter") String exporter,
			HttpServletResponse response) {
		try (CarIQAPI _getResource = new CarIQAPI("getExporterConstructor")) {
			return exporterService.getExporterDefinition(exporter);
		} catch (Exception e) {
			Utils.handleException(e);
		}
		return null;
	}

	/**
	 * Export Single record.
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response is a file
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Export One Record", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/exportSingle/{exporter}/{queryType}", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public GenericJSON exportSingle(
			@CarIQPublicAPIParameter(name = "exporter", description = "the exporter") @PathVariable("exporter") String exporter,
			@CarIQPublicAPIParameter(name = "queryType", description = "the queryType") @PathVariable("queryType") String queryType,
			@RequestBody GenericJSON json, HttpServletResponse response) {
		try (CarIQAPI _addResource = new CarIQAPI("CARIQ_exportSingle")) {
			try {
				return exporterService.exportSingle(exporter, queryType, json);
			} catch (Exception e) {
				Utils.handleException(e);
			}
		}

		return null;
	}

	/**
	 * Export Single record.
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response is a file
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Export Multiple Records", responseClass = List.class, internal = true)
	@RequestMapping(value = "/exportMultiple/{exporter}/{queryType}/{pageNumber}/{pageSize}", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public List<GenericJSON> exportMultiple(
			@CarIQPublicAPIParameter(name = "exporter", description = "the exporter") @PathVariable("exporter") String exporter,
			@CarIQPublicAPIParameter(name = "queryType", description = "the queryType") @PathVariable("queryType") String queryType,
			@CarIQPublicAPIParameter(name = "pageNumber", description = "the pageNumber") @PathVariable("pageNumber") int pageNumber,
			@CarIQPublicAPIParameter(name = "pageSize", description = "the pageSize") @PathVariable("pageSize") int pageSize,
			@RequestBody GenericJSON json, HttpServletResponse response) {
		try (CarIQAPI _addResource = new CarIQAPI("CARIQ_exportMulti")) {
			try {
				return exporterService.exportMultiple(exporter, queryType, json, pageNumber, pageSize);
			} catch (Exception e) {
				Utils.handleException(e);
			}
		}

		return null;
	}

	/**
	 * Populate values into the file
	 *
	 * @param param
	 *            the input file
	 * @param response
	 *            the response is a file
	 * @return the object
	 */
	@RequestMapping(value = "/populate/{exporter}/{queryType}", method = RequestMethod.POST, consumes = {
			"multipart/form-data" })
	@ResponseBody
	@Transactional
	public void populate(@RequestParam("csvfile") MultipartFile csvFile, @PathVariable("exporter") String exporter,
			@PathVariable("queryType") String queryType, HttpServletResponse response) {
		try (CarIQAPI _addResources = new CarIQAPI("CARIQ_export")) {
			try {
				File file = fileHandlerService.getLocalFileHandle(csvFile, exporter + ".csv");
				exporterService.pupulateFile(exporter, queryType, file, response);
			} catch (Exception e) {
				Utils.handleException(e);
			}
		}
	}

	/**
	 * Export records in a file.
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response is a file
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Download csv with given exporter", responseClass = File.class, internal = true)
	@RequestMapping(value = "/export/{exporter}/{queryType}", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public void export(
			@CarIQPublicAPIParameter(name = "exporter", description = "the exporter") @PathVariable("exporter") String exporter,
			@CarIQPublicAPIParameter(name = "queryType", description = "the queryType") @PathVariable("queryType") String queryType,
			@RequestBody GenericJSON json, HttpServletResponse response) {
		try (CarIQAPI _addResource = new CarIQAPI("CARIQ_export_POST")) {
			try {
				exporterService.export(exporter, queryType, json, "csv", response);
			} catch (Exception e) {
				Utils.handleException(e);
			}
		}
	}

	/**
	 * Export records in a file.
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response is a file
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Export into a file in given type", responseClass = Object.class, internal = true)
	@RequestMapping(value = "/export/{exporter}/{queryType}/{fileType}", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public void export(
			@CarIQPublicAPIParameter(name = "exporter", description = "the param") @PathVariable("exporter") String exporter,
			@CarIQPublicAPIParameter(name = "queryType", description = "the queryType") @PathVariable("queryType") String queryType,
			@CarIQPublicAPIParameter(name = "fileType", description = "the fileType") @PathVariable("fileType") String fileType,
			@RequestBody GenericJSON json, HttpServletResponse response) {
		try (CarIQAPI _addResource = new CarIQAPI("CARIQ_export_fileType_POST")) {
			try {
				exporterService.export(exporter, queryType, json, fileType, response);
			} catch (Exception e) {
				Utils.handleException(e);
			}
		}
	}

	/**
	 * Download records in a file.
	 *
	 * @param response
	 *            the response is a file
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Secured API Export the data in csv format String", responseClass = String.class, internal = true)
	@RequestMapping(value = "/download/{exporter}/{queryType}/{securityToken}", method = RequestMethod.GET)
	@ResponseBody
	@Transactional
	public Object download(
			@CarIQPublicAPIParameter(name = "exporter", description = "the exporter") @PathVariable("exporter") String exporter,
			@CarIQPublicAPIParameter(name = "queryType", description = "the queryType") @PathVariable("queryType") String queryType,
			@CarIQPublicAPIParameter(name = "securityToken", description = "the securityToken") @PathVariable("securityToken") String securityToken, HttpServletResponse response) {
		try (CarIQAPI _addResource = new CarIQAPI("CARIQ_export_GET")) {
			try {
				if (!configProperties.getProperty(Utils.SECURITY_TOKEN).equals(securityToken))
					Utils.handleException("Invalid Security Token");

				return exporterService.export(exporter, queryType, null, response);
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return null;
		}
	}
}
