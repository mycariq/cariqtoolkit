/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.SortableParameter;

/**
 * @author hrishi
 *
 */
public class DemoCompositeReportExporter extends MultiThreadedCompositeExporterTemplate {
	static final int COUNT = 10;

	private static GenericJSON addParameters(List<String> keys, int index) {
		GenericJSON retval = new GenericJSON();
		
		for (String key : keys) {
			retval.put(key, key + "-" + index);
		}
		
		return retval;
	}

	/**
	 * Return a, b, c, d1, e1, f1
	 * 
	 * @author hrishi
	 *
	 */
	static class DemoReportExporter1 extends CarIQExporterTemplate {

		/**
		 * @param name
		 * @param description
		 */
		public DemoReportExporter1() {
			super("DemoReportExporter1", "DemoReportExporter1");
			super.addExportAttributes(Arrays.asList("a", "b", "c", "d1", "e1", "f1"));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.
		 * lang.String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
			List<GenericJSON> jsonList = new ArrayList<GenericJSON>();
			for (int i = 5; i < COUNT + 5; i++) {
				jsonList.add(addParameters(Arrays.asList("a", "b", "c", "d1", "e1", "f1"), i));
			}

			return jsonList;
		}

	}

	static class DemoReportExporter2 extends CarIQExporterTemplate {

		/**
		 * Return a, b, c, d2, e2, f2
		 * 
		 * @param name
		 * @param description
		 */
		public DemoReportExporter2() {
			super("DemoReportExporter2", "DemoReportExporter2");
			super.addExportAttributes(Arrays.asList("a", "b", "c", "d2", "e2", "f2"));

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.
		 * lang.String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
			List<GenericJSON> jsonList = new ArrayList<GenericJSON>();
			for (int i = 0; i < COUNT; i++) {
				jsonList.add(addParameters(Arrays.asList("a", "b", "c", "d2", "e2", "f2"), i));
			}

			return jsonList;
		}

	}

	static class DemoReportExporter3 extends CarIQExporterTemplate {

		/**
		 * Return a, b, c, d3, e3
		 * 
		 * @param name
		 * @param description
		 */
		public DemoReportExporter3() {
			super("DemoReportExporter3", "DemoReportExporter2");
			super.addExportAttributes(Arrays.asList("a", "b", "c", "d3", "e3"));

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.
		 * lang.String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
			List<GenericJSON> jsonList = new ArrayList<GenericJSON>();
			for (int i = 7; i < COUNT+10; i++) {
				jsonList.add(addParameters(Arrays.asList("a", "b", "c", "d3", "e3"), i));
			}

			return jsonList;
		}

	}

	/**
	 * @param name
	 * @param description
	 */
	public DemoCompositeReportExporter() {
		super("DemoCompositeMultiThreaded", "Exporter that combines multiple exporters, runs in multiple threads and sorts and combines results");
		super.add(MultiThreadedCompositeExporterTemplate.EXPORTER, new DemoReportExporter1())
				.add(MultiThreadedCompositeExporterTemplate.EXPORTER, new DemoReportExporter2())
				.add(MultiThreadedCompositeExporterTemplate.EXPORTER, new DemoReportExporter3());
		
		super.add(MultiThreadedCompositeExporterTemplate.SORTABLE_PARAM, new SortableParameter("a", String.class))
		.add(MultiThreadedCompositeExporterTemplate.SORTABLE_PARAM, new SortableParameter("b", String.class))
		.add(MultiThreadedCompositeExporterTemplate.SORTABLE_PARAM, new SortableParameter("c", String.class));
	

		super.build();
	}

}
