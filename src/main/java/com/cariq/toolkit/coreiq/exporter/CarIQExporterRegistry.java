package com.cariq.toolkit.coreiq.exporter;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CarIQExporterRegistry {
	static Map<String, CarIQExporter> map = new HashMap<String, CarIQExporter>();
	
	static void add(CarIQExporter exporter) {
		map.put(exporter.getName().toUpperCase(), exporter);
	}
	static {
		// Enlist all the exporters to be made available to Admin through REST call
//		add(new CarDetailsExporter());
//		add(new LitmusReportExporter());
//		add(new ShipmentOrderExporter());
//		add(new PidExporter());
//		add(new UnshippedOrdersExporter());
//		// Exporter to report everything about Shipped/unshipped orders
//		add(new InsuranceShipmentCarUserDevicePacketReportExporter());
//		add(new SystemReportExporter());
//		add(new PlatformActivityReportExporter());
//		add(new ShipmentActivityReportExporter());
//		
//		// Trends Exporters
//		add(new PieChartTrendExporterTemplate(new AlertTypesTrendExporter()));
//		add(new CarsDistanceTrendExporter());
//		add(new CarsMoneyTrendExporter());
//		add(new CarsPerformanceTrendExporter());
//		add(new CarsTrendExporter());
//		add(new PolicyCarsConversionFunnelTrendExporter());
//		add(new EngagementTrendExporter());
//		
//		// Architectural and System Trends Exporters
//		add(new PieChartTrendExporterTemplate(new ClientTypesTrendExporter()));
//		add(new DataSizeTrendExporter());
//		add(new ThroughputTrendExporter());
//		add(new CrawlerTimeTrendExporter());
//		
//		// Static Data Exporter and Loader
//		// add(new MakeModelInUseExporter());
//		add(new ReportedErrorsExporter());
//
//		// Logistics 
//		add(new LogisticsEmailBroadcaster());		
//		
		// Demo multi-threaded
		add(new DemoCompositeReportExporter());
	}

	static CarIQExporter getExporter(String exporterName) {
		return map.get(exporterName.toUpperCase());
	}
	
	static Collection<CarIQExporter> getAllExporters() {
		return map.values();
	}
}
