package com.cariq.toolkit.coreiq.exporter;

public class QueryDefinitionAttribute {
	String name;
	String description;
	Class clazz;
	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	public Class getClazz() {
		return clazz;
	}
	public QueryDefinitionAttribute(String name, String description, Class clazz) {
		super();
		this.name = name;
		this.description = description;
		this.clazz = clazz;
	}
	
	public QueryDefinitionAttribute(String name) {
		this(name, name);
	}
	
	public QueryDefinitionAttribute(String name, String description) {
		this(name, description, String.class);
	}
}