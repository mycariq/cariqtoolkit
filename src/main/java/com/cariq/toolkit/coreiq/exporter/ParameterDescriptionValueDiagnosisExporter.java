/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.HtmlRenderingRuleCondition;
import com.cariq.toolkit.utils.HtmlRenderingRuleCondition.ConditionType;
import com.cariq.toolkit.utils.HtmlRenderingValue;
import com.cariq.toolkit.utils.HtmlRuleBasedRenderer;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.ObjectBuilder;
import com.cariq.toolkit.utils.SimpleHtmlRenderingRule;
import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi
 *
 */
public abstract class ParameterDescriptionValueDiagnosisExporter extends CarIQExporterTemplate implements ObjectBuilder<ParamValueExporterTask> {
	List<ParamValueExporterTask> exporterTasks = new ArrayList<ParamValueExporterTask>();
	
	/**
	 * @param name
	 * @param description
	 */
	public ParameterDescriptionValueDiagnosisExporter(String name, String description) {
		super(name, description);

		// Declare Export attributes
		addExportAttributes(Arrays.asList(Utils.PARAMETER, Utils.DESCRIPTION, Utils.VALUE, Utils.DIAGNOSIS));

	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.String, com.cariq.www.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		int startCount = Utils.validatePagination(pageSize, pageNo);
		List<GenericJSON> exportList = new ArrayList<GenericJSON>();
		// Execute Exporter Tasks - given by the pageNo and PageSize
		ExecutorService executorService = Executors.newFixedThreadPool(10);

		try {
			List<Future<ParamDescriptionValueDiagnosis>> futures = new ArrayList<Future<ParamDescriptionValueDiagnosis>>();

			// take into account pageNumber too
			int taskCount = Math.min(startCount + pageSize, exporterTasks.size());
			for (int taskIndex = startCount; taskIndex < taskCount; taskIndex++) {
				ParamValueExporterTask exporterTask = exporterTasks.get(taskIndex);
				if (exporterTask == null)
					break;

				futures.add(executorService.submit(exporterTask.initialize(queryType, json)));
			} 
			
			// Now iterate over futures to get the output
			for (Future<ParamDescriptionValueDiagnosis> future : futures) {
				ParamDescriptionValueDiagnosis retval;
				try {
					retval = future.get();
					exportList.add(retval.toJSON());
				} catch (Exception e) {
					Utils.logException(logger, e, "ParamValueExporterTasks");
				}
			}			
		} finally {
			executorService.shutdown();
		}
		
		return exportList;
	}

	@Override
	public HtmlValueRenderer getHtmlRenderer() {
		HtmlRuleBasedRenderer renderer = new HtmlRuleBasedRenderer();

		// make First name bold
		renderer.add(new SimpleHtmlRenderingRule(Utils.DIAGNOSIS,
				new HtmlRenderingRuleCondition(ConditionType.Equal, Utils.DIAG_GOOD ), HtmlRenderingValue.HtmlGreen));
		renderer.add(new SimpleHtmlRenderingRule(Utils.DIAGNOSIS,
				new HtmlRenderingRuleCondition(ConditionType.Equal, Utils.DIAG_OK ), HtmlRenderingValue.HtmlYellow));
		renderer.add(new SimpleHtmlRenderingRule(Utils.DIAGNOSIS,
				new HtmlRenderingRuleCondition(ConditionType.Equal, Utils.DIAG_BAD ), HtmlRenderingValue.HtmlRed));

		return renderer;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.ObjectBuilder#configure(java.lang.String, java.lang.Object)
	 */
	@Override
	public ParameterDescriptionValueDiagnosisExporter configure(String setting, Object value) {
		return this;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.ObjectBuilder#add(java.lang.Object)
	 */
	@Override
	public ParameterDescriptionValueDiagnosisExporter add(ParamValueExporterTask task) {
		exporterTasks.add(task);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.ObjectBuilder#add(java.lang.String, java.lang.Object)
	 */
	@Override
	public ParameterDescriptionValueDiagnosisExporter add(String parameter, ParamValueExporterTask task) {
		exporterTasks.add(task);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.ObjectBuilder#build()
	 */
	@Override
	public ParameterDescriptionValueDiagnosisExporter build() {
		return this;
	}
}
