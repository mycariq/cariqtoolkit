/**
 * 
 */
package com.cariq.toolkit.coreiq.loaderexporter;

import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterDefinition;
import com.cariq.toolkit.coreiq.exporter.CarIQQueryDefinition;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderDefinition;
import com.cariq.toolkit.utils.CarIQAttrDefinition;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.HtmlValueIdentityRenderer;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Pair of Loader and Exporter
 * Whatever Exporter Exports, Loader should be able to Load
 * Examples
 *  PID export and Load
 *  Mailer exporter and Load
 *  Maybe more
 * 
 * @author hrishi
 *
 */
public abstract class CarIQLoaderExporterTemplate implements CarIQLoaderExporter {
	protected static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQLoaderExporter");
	CarIQExporterDefinition exporterDefinition;
	CarIQLoaderDefinition loaderDefinition;

	// Abstract methods to be implemented by Derived classes
	protected abstract void doLoad(GenericJSON json, CarIQLoaderContext context);
	protected abstract List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize);

	
	/**
	 * Construct the base
	 */
	public CarIQLoaderExporterTemplate(String name, String description) {
		exporterDefinition = new CarIQExporterDefinition(name, description);
		loaderDefinition = new CarIQLoaderDefinition(name, description);
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.loader.CarIQLoader#getName()
	 */
	@Override
	public String getName() {
		return loaderDefinition.getName();
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.loader.CarIQLoader#getDescription()
	 */
	@Override
	public String getDescription() {
		return loaderDefinition.getDescription();
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.loader.CarIQLoader#getAttributes()
	 */
	@Override
	public List<CarIQAttrDefinition> getAttributes() {
		return loaderDefinition.getAttrDefinition();
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.loader.CarIQLoader#load(com.cariq.www.utils.GenericJSON, com.cariq.www.coreiq.loader.CarIQLoaderContext)
	 */
	@Override
	public void load(GenericJSON json, CarIQLoaderContext context) {
		try (ProfilePoint _Loader_load = ProfilePoint.profileAction("ProfAction_Loader_load_" + this.getClass().getName())) {
			logger.debug("Loader: " + this.getClass().getName() + " Input: " + json.toString());
			
			CarIQAttrDefinition.validateJSON(json, getAttributes());
			doLoad(json, context);
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.loader.CarIQLoader#getLoaderDefinition()
	 */
	@Override
	public CarIQLoaderDefinition getLoaderDefinition() {
		return loaderDefinition;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#export(java.lang.String, com.cariq.www.utils.GenericJSON, int, int)
	 */
	@Override
	public List<GenericJSON> export(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _exporter_export = ProfilePoint.profileActivity("ProfActivity_exporter_export_" + this.getClass().getName())) {
			json = Utils.trimStringValues(json);
			return doExport(queryType, json, pageNo, pageSize);
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#getExporterDefinition()
	 */
	@Override
	public CarIQExporterDefinition getExporterDefinition() {
		return exporterDefinition;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#getHtmlRenderer()
	 */
	@Override
	public	HtmlValueRenderer getHtmlRenderer() {
		return new HtmlValueIdentityRenderer();
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#isAuthorised()
	 */
	@Override
	public boolean isAuthorised() {
		return exporterDefinition.isAuthorized();
	}
	
	// ------------------
	// Protected methods for Loader and Exporter for Attributes and Roles
	// ------------------
	protected final void addAttribute(String name, String description) {
		loaderDefinition.addAttrDefinition(new CarIQAttrDefinition(name, description));
		exporterDefinition.add(CarIQExporterDefinition.EXPORT_PARAM, name);
	}
	
	protected final void addExporterSupportedRole(String supportedRole) {
		exporterDefinition.add(CarIQExporterDefinition.ROLE, supportedRole);
	}
	
	protected final void addSupportedRoles(List<String> supportedRoles) {
		for (String name : supportedRoles) {
			addExporterSupportedRole(name);
		}
	}
	
	protected final void addQueryType(String name, String description, String[] attrList) {
		exporterDefinition.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(name, description, attrList));
	}
	

	protected final void addQueryType(String name, String description, List<QueryDefinitionAttribute> attrList) {
		exporterDefinition.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(name, description, attrList));
	}
}
