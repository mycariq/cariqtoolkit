package com.cariq.toolkit.coreiq.composite;

import org.springframework.stereotype.Service;

@Service
public class CompositeAPIServiceImpl implements CompositeAPIService {

	@Override
	public Object call(APIGroup group, String auth) {
		return group.execute(auth);
	}
}
