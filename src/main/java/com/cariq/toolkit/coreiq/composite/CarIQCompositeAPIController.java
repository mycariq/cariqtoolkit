package com.cariq.toolkit.coreiq.composite;

import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;

@CarIQPublicAPI(name = "CompositeAPI", description = "API to call multiple APIs in one call")
@RequestMapping("/composite")
@Controller
public class CarIQCompositeAPIController {

	@Autowired
	CompositeAPIService compositeAPIService;

	@Resource(name = "configProperties")
	private Properties configProperties;

	/**
	 * Calls APIs given in the json and returns JSONs categorized in the same
	 * manner Uses Basic Auth supplied to the parent
	 *
	 * @param param
	 *            the json containing the list of API
	 * @param response
	 *            the response to each of the API
	 * @return
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Call set of APIs", responseClass = GenericJSON.class)
	@RequestMapping(value = "/call", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public Object call(@RequestHeader("Authorization") String auth, @RequestBody APIGroup group,
			HttpServletResponse response) {
		try (CarIQAPI _addResource = new CarIQAPI("CARIQ_composite_api")) {
			try {
				return compositeAPIService.call(group, auth);
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return null;
		}
	}

	@RequestMapping(value = "/call1", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public Object call1(@RequestHeader("Authorization") String auth, @RequestBody GenericJSON group,
			HttpServletResponse response) {
		try (CarIQAPI _addResource = new CarIQAPI("CARIQ_composite_api")) {
			try {
				return new ResponseJson("Called Call1 with input JSON: " + group.toString());
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return null;
		}
	}
}
