# README #


### What is this repository for? ###

* Contains Portable Java Toolkit for Webservice development

### Version History
 * Version History (Replicated in README.md too)
 * 1.0.1, 6-March-2017 - First Port
 * 1.0.2, 8-March-2017 - Add Anomaly,Action logging and publicAPI fix for Boolean
 * 1.0.3, 9-March-2017 - Add RestAPIClientService for calling REST API
 

### How do I get set up? ###

* Clone the Repo
* Physically copy the directory under com/cariq/tookit into the repository (later we'll have some automation)
* Build
* As far as possible, do not make any change to any file under com.cariq.toolkit package.
* If any changes are done to toolkit files, make changes into the Toolkit repository and bump up Toolkit version under com.cariq.toolkit.ToolkitVersion - also add into versionHistory about the changes done
* While doing first time, do the following customizations in log4j.properties, applicationContext.xml etc to make end to end API and Profile logging work
* Periodically copy the toolkit - by overriding the existing one. If the new version has something good. 

## Add controller names to the PublicAPIDocumentGenerator as follows:
	static {
		controllers.add(TestController.class);
		controllers.add(LocationServiceController.class);
	}
	
## Make change to PublicAPIBuilder.java to write the root path of the webservice
    private static String SERVER_BASE_PATH = "/ConnectIQ"; //Must be overridden by the webservice - to be automated later
    
## Copy and paste the Controller APIDocController.java from toolkit/publicAPI folder to www.web folder of the webservice (where rest of the controllers reside)

## Add following Lines to log4j.properties CarIQ Toolkit logging

 ### Toolkit Logging
log4j.appender.TKLOG=org.apache.log4j.RollingFileAppender
log4j.appender.TKLOG.File=${logPath}/logs/cariq-toolkit.csv
log4j.appender.TKLOG.layout=org.apache.log4j.PatternLayout
log4j.appender.TKLOG.layout.ConversionPattern=%d %-5p,%m%n
 ### Keep 5 MB file for ease of opening with excel
log4j.appender.TKLOG.MaxFileSize=5MB
 ### Keep 100 backup file
log4j.appender.TKLOG.MaxBackupIndex=100

 ### ProfilePoint log messages
log4j.appender.PP=org.apache.log4j.RollingFileAppender
log4j.appender.PP.File=${logPath}/logs/cq_profilepoint.csv
log4j.appender.PP.layout=org.apache.log4j.PatternLayout
log4j.appender.PP.layout.ConversionPattern=%d %-5p,%m%n
 ### Keep 5 MB file for ease of opening with excel
log4j.appender.PP.MaxFileSize=5MB
 ### Keep one backup file
log4j.appender.PP.MaxBackupIndex=20


 ### TKLOG Logging
log4j.logger.CarIQToolkit=DEBUG, TKLOG
 ### ProfilePoint Logging
log4j.logger.ProfilePoint=DEBUG, PP


## Add following Dependencies to pom.xml
		<!--  Toolkit Dependencies -->
		
		<dependency>
			<groupId>org.apache.velocity</groupId>
			<artifactId>velocity-tools</artifactId>
			<version>2.0</version>
		</dependency>
		
				<dependency>
			<groupId>org.json</groupId>
			<artifactId>json</artifactId>
			<version>20090211</version>
		</dependency>

		<!-- Azure storage maven dependency -->
		<dependency>
			<groupId>com.microsoft.azure</groupId>
			<artifactId>azure-storage</artifactId>
			<version>2.2.0</version>
		</dependency>
		
		<!-- httpclient to handle ssl certificate -->
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpcore</artifactId>
			<version>${httpcore.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>commons-logging</artifactId>
					<groupId>commons-logging</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<!-- http component to create raw HTTP calls -->
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>${httpclient.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>commons-logging</artifactId>
					<groupId>commons-logging</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<!-- Timing library  -->
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>2.2</version>
		</dependency>
## Changes to applicationContext.xml 
### Add following in the properties sections (at top)
	<httpcore.version>4.4.3</httpcore.version>
	<httpclient.version>4.5.1</httpclient.version>
	
### to add following line for CorsFilter in beans section
	<bean id="corsHandler" class="com.cariq.toolkit.utils.CorsFilter" />


### Also add scan package as com
	<context:component-scan base-package="com">

### Add Velocity Bean in Beans Section

	<bean id="velocityEngine"
		class="org.springframework.ui.velocity.VelocityEngineFactoryBean">
		<property name="velocityProperties">
			<value>
				resource.loader=class
				class.resource.loader.class=org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader
			</value>
		</property>
	</bean>

## Who do I talk to? ###


* hrishi@mycariq.com
